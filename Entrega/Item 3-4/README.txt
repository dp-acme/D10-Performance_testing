Los Items 3 y 4 vienen incluidos en la misma carpeta, ya que los tests est�n incluidos en el mismo proyecto. 

Para la configuraci�n del protocolo HTTPS
se debe de seguir la gu�a descrita en nuestro entregable D08 - Lessons Learnt.

Las modificaciones necesarias para implementar dicho protocolo
se realizan sobre los archivos de configuraci�n del servidor, 
debido a esto en el proyecto no se refleja ning�n cambio con respecto a la versi�n sin HTTPS.

Las diferentes b�squedas implementadas en el proyecto se han realizado �nicamente sobre los objetos publicados y p�blicos,
ya que no se espec�fica la misma, para simplificar la implementaci�n de las mismas.