package services;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.mchange.v1.util.UnexpectedException;

import utilities.AbstractTest;
import domain.Newspaper;
import domain.Subscription;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class SubscriptionServiceTest extends AbstractTest{

	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	@Autowired
	private CustomerService customerService;
	
/*
	22. An actor who is authenticated as a customer can:
		1. Subscribe to a private newspaper by providing a valid credit card.*/
	@Test
	public void driverCreate(){
		Object[][] testingData = {
				//Positivos
				{"customer3", "newspaper2", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, null},
				
				//Negativos
				{"customer1", "newspaper2", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, IllegalArgumentException.class}, //Rendezvous draft
				{"user3", "newspaper2", "4485 3510 6394 0939", "holderName", "brandName", 123, 12, 20, IllegalArgumentException.class}, //Wrong user
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateCreate((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2],
					(String) testingData[i][3], (String) testingData[i][4], (Integer) testingData[i][5], (Integer) testingData[i][6],
					(Integer) testingData[i][7], (Class<?>) testingData[i][8]);
		}
	}
	
/*25. Private newspapers can be fully displayed by customers who have subscribed to them. If a
		private newspaper is shown to an actor who is not a customer who has subscribed to it, then
		only the table of contents must be shown. Simply put, no navigation from the table of contents
		to the articles must be allowed.*/
	@Test
	public void driverList(){
		Object[][] testingData = {
				//Positivos
				{"customer1", "subscription1", null},
				
				//Negativos
				{"customer1", "subscription2", UnexpectedException.class}, 	
				{"user1", "subscription2", NullPointerException.class}, 
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateList((String) testingData[i][0],(String) testingData[i][1],(Class<?>) testingData[i][2]);
		}
	}

	protected void templateCreate(String username, String newspaperBeanId, String creditCardNumber,
			String holderName, String brandName, Integer cvv, Integer expirationMonth, Integer expirationYear, Class<?> expected){
		Class<?>  caught;
		
		caught = null;
		
		try{			
			super.startTransaction();
			
			super.authenticate(username);
			
			Subscription subscription;
			Newspaper newspaper;
			
			newspaper = newspaperService.findOne(super.getEntityId(newspaperBeanId));
			
			subscription = subscriptionService.create(newspaper);
			
			subscription.setBrandName(brandName);
			subscription.setHolderName(holderName);
			subscription.setCreditCardNumber(creditCardNumber);
			subscription.setExpirationMonth(expirationMonth);
			subscription.setExpirationYear(expirationYear);
			subscription.setCvv(cvv);
			
			subscriptionService.save(subscription);
			subscriptionService.flush();
			
			super.unauthenticate();	
		}catch (Throwable e) {
			caught = e.getClass();
		}finally{
			rollbackTransaction();
		}
		
		checkExceptions(expected, caught);
	}
	

	
	protected void templateList(String username, String subscriptionBeanId, Class<?> expected){
		Class<?>  caught;
		Subscription subscription;
		int subscriptionId, userId;
		List<Subscription> subscriptions = new ArrayList<Subscription>();
		
		caught = null;

		try{
			this.startTransaction();
			
			super.authenticate(username);
			subscriptionId = super.getEntityId(subscriptionBeanId);
			subscription = subscriptionService.findOne(subscriptionId);
			userId = super.getEntityId(username);
					
			subscriptions = (List<Subscription>) subscriptionService.getSubscriptionsFromCustomer(customerService.findOne(userId).getId());
					
			if(!subscriptions.contains(subscription)){
				throw new UnexpectedException();
			}
			super.unauthenticate();
			
		}catch (Throwable e) {
			caught = e.getClass();
		}finally{
			this.rollbackTransaction();
		}
		
		checkExceptions(expected, caught);
	}
}
