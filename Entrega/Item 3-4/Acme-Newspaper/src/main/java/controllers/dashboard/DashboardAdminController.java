/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import controllers.AbstractController;

@Controller
@RequestMapping("/dashboard")
public class DashboardAdminController extends AbstractController {

	// Services -----------------------------------------------------------
	
	@Autowired
	AdministratorService administratorService;
	
	// Constructors -----------------------------------------------------------

	public DashboardAdminController() {
		super();
	}

	@RequestMapping("/display")
	public ModelAndView display() {
		String[] getAvgSdNewspapersPerUser, getAvgSdArticlesPerUser, getAvgSdArticlesPerNewspaper,
		getAvgSdChirpsPerUser;
		
		Integer round;
		
		round = 2;
		
		String ratioOfCreators, ratioOfArticleCreators,
		ratioUsersWithOver75PercentAvgChirps,ratioPublicOverPrivateNewspapers, 
		getAvgArticlesPerPrivateNewspaper, getAvgArticlesPerPublicNewspaper,
		getAvgRatioPrivateOverPublicNewspaperPerUser,getAvgFollowUpsPerArticle, 
		getAvgFollowupsPerArticlePlus1Week, getAvgFollowupsPerArticlePlus2Weeks;
		
		ModelAndView result;

		result = new ModelAndView("dashboard");
		
		getAvgSdNewspapersPerUser = administratorService.roundTo(administratorService.getAvgSdNewspapersPerUser(), round);
		getAvgSdArticlesPerUser = administratorService.roundTo(administratorService.getAvgSdArticlesPerUser(), round);
		getAvgSdArticlesPerNewspaper = administratorService.roundTo(administratorService.getAvgSdArticlesPerNewspaper(), round);
		ratioOfCreators = administratorService.roundTo(administratorService.ratioOfCreators(), round);
		ratioOfArticleCreators = administratorService.roundTo(administratorService.ratioOfArticleCreators(), round);
		getAvgFollowUpsPerArticle = administratorService.roundTo(administratorService.getAvgFollowUpsPerArticle(), round);
		
		getAvgSdChirpsPerUser = administratorService.roundTo(administratorService.getAvgSdChirpsPerUser(), round);
		ratioUsersWithOver75PercentAvgChirps = administratorService.roundTo(administratorService.ratioUsersWithOver75PercentAvgChirps(), round);

		ratioPublicOverPrivateNewspapers = administratorService.roundTo(administratorService.ratioPublicOverPrivateNewspapers(),round);
		getAvgArticlesPerPrivateNewspaper = administratorService.roundTo(administratorService.getAvgArticlesPerPrivateNewspaper(), round);
		getAvgArticlesPerPublicNewspaper = administratorService.roundTo(administratorService.getAvgArticlesPerPublicNewspaper(),round);
		getAvgRatioPrivateOverPublicNewspaperPerUser = administratorService.roundTo(administratorService.getAvgRatioPrivateOverPublicNewspaperPerUser(),round);
		getAvgFollowupsPerArticlePlus1Week = administratorService.roundTo(administratorService.getAvgFollowupsPerArticlePlus1Week(),round);
		getAvgFollowupsPerArticlePlus2Weeks = administratorService.roundTo(administratorService.getAvgFollowupsPerArticlePlus2Weeks(),round);

		
		result.addObject("getAvgSdNewspapersPerUser", getAvgSdNewspapersPerUser);
		result.addObject("getAvgSdArticlesPerUser", getAvgSdArticlesPerUser);
		
		result.addObject("getAvgSdArticlesPerNewspaper", getAvgSdArticlesPerNewspaper);
		result.addObject("ratioOfCreators", ratioOfCreators);
		result.addObject("ratioOfArticleCreators", ratioOfArticleCreators);
		result.addObject("getAvgSdChirpsPerUser", getAvgSdChirpsPerUser);
		result.addObject("ratioUsersWithOver75PercentAvgChirps", ratioUsersWithOver75PercentAvgChirps);
		result.addObject("ratioPublicOverPrivateNewspapers", ratioPublicOverPrivateNewspapers);
		result.addObject("getAvgArticlesPerPrivateNewspaper", getAvgArticlesPerPrivateNewspaper);
		result.addObject("getAvgFollowUpsPerArticle", getAvgFollowUpsPerArticle);

		result.addObject("getAvgArticlesPerPublicNewspaper", getAvgArticlesPerPublicNewspaper);
		result.addObject("getAvgRatioPrivateOverPublicNewspaperPerUser", getAvgRatioPrivateOverPublicNewspaperPerUser);
		
		result.addObject("getAvgFollowupsPerArticlePlus1Week", getAvgFollowupsPerArticlePlus1Week);
		result.addObject("getAvgFollowupsPerArticlePlus2Weeks", getAvgFollowupsPerArticlePlus2Weeks);
		
		result.addObject("getNewspaperWith10PercentLessArticlesThanAvg", administratorService.getNewspaperWith10PercentLessArticlesThanAvg());
		result.addObject("getNewspaperWith10PercentMoreArticlesThanAvg", administratorService.getNewspaperWith10PercentMoreArticlesThanAvg());
		result.addObject("ratioOfSubscribersPerNewspaper", administratorService.ratioOfSubscribersPerNewspaper());

		return result;
	}

}