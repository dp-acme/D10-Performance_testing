package services;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ArticleRepository;
import domain.Actor;
import domain.Administrator;
import domain.Article;
import domain.Newspaper;
import domain.User;

@Service
@Transactional
public class ArticleService {
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private ArticleRepository articleRepository;
	
	// Validator ---------------------------------------------------

	@Autowired
	Validator validator;
	
	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	@Autowired
	private ConfigurationService configurationService;

	// Constructor ---------------------------------------------------
	
	public ArticleService(){
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Article create(Article article, Newspaper newspaper){
		Article result;
		
		result = new Article();
		result.setArticle(article);
		result.setNewspaper(newspaper);
		result.setDraft(article == null);
		result.setMoment(article == null? null : new Date());
		result.setPictures(new ArrayList<String>());
		
		return result;
	}
	
	public Article findOne(Integer articleId){
		Article result;
		
		result = articleRepository.findOne(articleId);
		
		return result;
	}
	
	public Collection<Article> findAll(){
		Collection<Article> result;
		
		result = articleRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Article save(Article article){
		Article result;
		Actor principal;
		
		Assert.notNull(article);
		Assert.isTrue(article.getArticle() == null ^ article.getNewspaper() == null); //O es un art�culo o es un follow-up
		Assert.isTrue(article.getArticle() == null || !article.getDraft()); //Si es un follow-up no debe estar en draft
		Assert.isTrue(article.getArticle() == null || article.getArticle().getMoment() != null && article.getArticle().getMoment().before(article.getMoment())); //Si es un follow-up debe tener una fecha posterior a la del art�culo
		Assert.isTrue(article.getArticle() == null || article.getArticle().getNewspaper() != null); //Un follow-up debe serlo de un art�culo, no de otro follow-up
		Assert.isTrue(article.getArticle() == null || !article.getArticle().getDraft()); //Un follow-up debe serlo de un art�culo final
		Assert.isTrue(article.getMoment() == null || !article.getDraft()); //Si est� en draft la fecha debe ser nula

		if(article.getId() == 0){ //Si se est� creando
			Assert.isTrue(article.getNewspaper() == null || article.getNewspaper().isDraft()); //Un art�culo debe crearse en un newspaper draft
		
		} else{ //Si se est� editando
			Assert.isTrue(article.getDraft()); //Solo se pueden editar art�culos en draft
		}
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof User);		

		if(article.getNewspaper() != null){ //Si se trata de un art�culo
			Assert.isTrue(article.getNewspaper().getCreator().equals((User) principal)); //El que est� logueado es el creador del peri�dico
		
		} else{ //Si se trata de un follow-up
			Assert.isTrue(article.getArticle().getNewspaper().getCreator().equals((User) principal)); //El que est� logueado es el creador	del peri�dico que completa
		}	
		
		article.setContainsTabooWord(configurationService.hasTabooWords(article.getTitle()) || 
									 configurationService.hasTabooWords(article.getSummary()) ||
									 configurationService.hasTabooWords(article.getBody())); //Detecci�n de palabras tab�
		
		result = articleRepository.save(article);
		
		return result;
	}
	
	public void delete(Article article){
		Assert.notNull(article);
		Assert.notNull(article.getNewspaper());
		Assert.isTrue(!article.getDraft());
		
		Collection<Article> followUps;
		Actor principal;	
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Administrator);		
		
		followUps = articleRepository.findFollowUpsByArticleId(article.getId());
		
		articleRepository.deleteInBatch(followUps);
		articleRepository.delete(article);
	}
	
	// Other business methods -------------------------------------------------
	
	public Article reconstruct(Article prunnedArticle, BindingResult binding, Integer newspaperId, Integer articleId){
		Article result;
		
		Assert.isTrue(newspaperId == null ^ articleId == null);
		
		if(prunnedArticle.getId() == 0){ //Si se est� creando
			result = prunnedArticle;
			
			if(articleId != null){ //Si es un follow-up
				Article article;
				
				article = this.findOne(articleId);
				Assert.notNull(article);
				
				result.setArticle(article);
				
			} else{ //Si es un article
				Newspaper newspaper;
				
				newspaper = newspaperService.findOne(newspaperId);
				Assert.notNull(newspaper);
				
				result.setNewspaper(newspaper);				
			}
			
			result.setDraft(articleId == null);
			result.setMoment(articleId == null? null : new Date());
			result.setContainsTabooWord(false);
			
			if(result.getPictures() == null){
				result.setPictures(new ArrayList<String>());
			}
				
		} else{			
			Article oldArticle;
			
			oldArticle = this.findOne(prunnedArticle.getId());
			Assert.notNull(oldArticle);
			Assert.notNull(oldArticle.getNewspaper());
			
			result = prunnedArticle;

			result.setDraft(true);
			result.setContainsTabooWord(false);
			
			if(result.getPictures() == null){
				result.setPictures(new ArrayList<String>());
			}
			
			result.setNewspaper(oldArticle.getNewspaper());
		}
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public Boolean reconstructPictures(Article article, String pictures) {
		Collection<String> result;
				
		if(pictures != null && !pictures.isEmpty()){
			result = new ArrayList<>();
			
			String[] urls = pictures.split("\n");
			
			for(String url : urls){
				if(!url.isEmpty()){
					try{
						new URL(url);
						
						result.add(url);
						
					} catch (MalformedURLException oops) {
						return false;
					}	
				}
			}
			
			article.setPictures(result);
		}
		
		return true;
	}
	
	public void putArticleInFinalMode(Article article) {
		Assert.notNull(article);
		Assert.notNull(article.getNewspaper());
		Assert.isTrue(article.getDraft());
		
		article.setDraft(false);
		
		articleRepository.save(article);
	}
	
	public void publishArticle(Article article) {
		Assert.notNull(article);
		Assert.notNull(article.getNewspaper());
		Assert.isTrue(!article.getDraft());
		
		article.setMoment(new Date());
		
		articleRepository.save(article);
	}
	
	public Collection<Article> findArticlesByNewspaperId(int newspaperId){
		Collection<Article> result;
		
		result = articleRepository.findArticlesByNewspaperId(newspaperId);
		Assert.notNull(result);
		
		return result;
	}
	
	public void deleteNewspaperArticles(int newspaperId){
		for(Article a:(articleRepository.findArticlesByNewspaperId(newspaperId))){
			this.delete(a);
		}
	}

	public Collection<Article> findFollowUpsByArticleId(Integer articleId) {
		Collection<Article> result;
		
		result = articleRepository.findFollowUpsByArticleId(articleId);
		Assert.notNull(result);
		
		return result;
	}

	public Collection<Article> searchArticlesByKeyword(String keyword) {
		Collection<Article> result;
		
		result = articleRepository.searchArticlesByKeyword("%" + keyword + "%");
		Assert.notNull(result);
		
		return result;
	}

	public Collection<Article> findPublishedArticles() {
		Collection<Article> result;
		
		result = articleRepository.findPublishedArticles();
		Assert.notNull(result);
		
		return result;
	}
	
	public void refreshContainsTabooWords(Collection<String> tabooWords) {
		Collection<Article> articles;
		
		articles = articleRepository.findAll();
		
		for (Article article : articles) {
			boolean result;
			String content;
			
			content = article.getTitle() + " " + article.getSummary() + " " + article.getBody();
			
			result = false;
			content = content.toLowerCase();
			for(String word : tabooWords) {
				word = word.toLowerCase();
				if(content.contains(word)){
					result = true;
					break;
				}
			}
			
			article.setContainsTabooWord(result);
		}		
	}
	
	public Collection<Article> articlesContainsTabooWords() {
		Collection<Article> result;
		Actor principal;

		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Administrator);
		
		result = articleRepository.articlesContainsTabooWords();
		Assert.notNull(result);
		
		return result;
	}

	public void flush() {
		articleRepository.flush();
	}


	
}
