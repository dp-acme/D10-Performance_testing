package forms;

public class ChirpForm {
	
	// Propiedades que vamos a mostrar en la vista
	private String editedTitle;
	private String editedDescription;
	
	public String getEditedTitle() {
		return editedTitle;
	}
	public void setEditedTitle(String editedTitle) {
		this.editedTitle = editedTitle;
	}
	public String getEditedDescription() {
		return editedDescription;
	}
	public void setEditedDescription(String editedDescription) {
		this.editedDescription = editedDescription;
	}
	
	
}
