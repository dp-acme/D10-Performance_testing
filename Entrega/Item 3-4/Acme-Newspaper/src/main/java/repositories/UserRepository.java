package repositories;


import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Article;
import domain.Chirp;
import domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	@Query("select u from User u where ?1 IN ELEMENTS (u.myFollowings)")
	List<User> getFollowers(User user);
	
	@Query("select a from Article a where a.newspaper.creator.id = ?1 AND a.draft = false")
	List<Article> getArticles(int userId);
	
	// Obtener los chirps de un user
	@Query("select c from Chirp c where c.user.id = ?1")
	Collection<Chirp> myChirps(int userId);

}