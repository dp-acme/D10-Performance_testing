<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<display:table name="subscriptions" id="row" requestURI="subscription/list.do" pagesize="5">
	
	<display:column property="newspaper.title" titleKey="subscription.list.title" sortable="false" href="newspaper/display.do?newspaperId=${row.getNewspaper().getId()}" />
	
	<display:column property="newspaper.description" titleKey="subscription.list.description" sortable="false" />
	
	<spring:message var="formatDate" code="subscription.list.formatDate" />
	<display:column property="newspaper.publicationDate" titleKey="subscription.list.publicationDate" sortable="true" format="{0,date,${formatDate}}" />
	
</display:table>
