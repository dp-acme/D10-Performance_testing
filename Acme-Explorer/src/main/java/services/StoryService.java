package services;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.StoryRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.ApplyFor;
import domain.Explorer;
import domain.Status;
import domain.Story;
import domain.Trip;

@Service
@Transactional
public class StoryService {
	
	// Managed repository --------------------------------------
	
	@Autowired
	private StoryRepository storyRepository;
	
	// Supporting services -------------------------------------
	
	@Autowired
	private ApplyForService applyForService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ExplorerService explorerService;
	
	// Constructors --------------------------------------------
	
	public StoryService(){
		super();
	}
	
	// Simple CRUD methods -------------------------------------
	
	public Story create(Explorer explorer, Trip trip) {
		Assert.notNull(explorer);
		Assert.notNull(trip);
		
		Story result;
		
		result = new Story();
		result.setExplorer(explorer);
		result.setTrip(trip);
		result.setAttachments(new ArrayList<String>());
		
		return result;
	}
	
	public Collection<Story> findAll() {
		Collection<Story> result;
		
		result = storyRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}

	public Story findOne(int storyId) {
		Assert.isTrue(storyId!=0);
		
		Story result;
		
		result = storyRepository.findOne(storyId);
		
		return result;
	}
	
	private static boolean urlCheck(String str){
		boolean res = true;
		
		try{
			new URL(str);
		} catch (MalformedURLException e) {
			res = false;
		}
		
		return res;
	}
	
	public Story save(Story story){
		Assert.notNull(story);
		
		Story result;
		UserAccount principal;
		Authority auth;
		ApplyFor applyFor;
		Explorer explorer;
		Trip trip;
		Status accepted;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.EXPLORER);
		
		if(story.getId() != 0){
			Assert.isTrue(story.getExplorer().getUserAccount().equals(principal));
		
		} else{
			Assert.isTrue(principal.getAuthorities().contains(auth));			
		}
		
		for(String str : story.getAttachments()){
			Assert.isTrue(urlCheck(str));
		}
		
		accepted = new Status();
		accepted.setStatus(Status.ACCEPTED);
		explorer = (Explorer)actorService.findByUserAccountId(principal.getId());
		trip = story.getTrip();
		applyFor = applyForService.getApplicationFromExplorerAndTrip(explorer, trip);
		Assert.isTrue(trip.getEndDate().before(new Date()));
				
		Assert.isTrue(applyFor.getStatus().getStatus().equals(Status.ACCEPTED));
		
		result = storyRepository.save(story);
		
		if (story.getId() == 0) {
			explorer.getStories().add(result);
			explorerService.save(explorer);
		}
		
		return result;
	}
	
	public Collection<Story> getStoriesPerTrip(int tripId){
		return storyRepository.getStoriesPerTrip(tripId);
	}
	
	// Other business methods -----------------------------------
}
