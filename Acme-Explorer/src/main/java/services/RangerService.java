package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.RangerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Folder;
import domain.Ranger;
import domain.SocialIdentity;

@Service
@Transactional
public class RangerService {

	// Managed repository --------------------------------------
	
	@Autowired
	private RangerRepository rangerRepository;

	// Supporting services -------------------------------------

	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	private ActorService actorService;
	
	// Constructor ---------------------------------------------------

	public RangerService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Ranger create() {
		
		Ranger result;

		result = new Ranger();
		
		result.setFolders(new ArrayList<Folder>());
		result.setSocialIdentities(new ArrayList<SocialIdentity>());
		result.setSuspicious(false);
		result.setBanned(false);
		
		return result;
	}

	public Ranger findOne(int rangerId) {
		Assert.isTrue(rangerId != 0);

		Ranger result;

		result = rangerRepository.findOne(rangerId);

		return result;
	}

	public Collection<Ranger> findAll() {
		Collection<Ranger> result;

		result = rangerRepository.findAll();

		Assert.notNull(result);

		return result;
	}

	public Ranger save(Ranger ranger) {
		Assert.notNull(ranger);
		
		Ranger result;
		UserAccount principal;
		

		if (ranger.getId() != 0) {
			principal = LoginService.getPrincipal();

			Assert.isTrue(ranger.getUserAccount().equals(principal));
		}else{
			String role;
			
			role=null;
			
			if(SecurityContextHolder.getContext().getAuthentication()!=null
					&&SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray().length>0){
			role = SecurityContextHolder.getContext()
					.getAuthentication().getAuthorities().toArray()[0].toString();
			}
			
			if(role!=null&&!role.equals("ROLE_ANONYMOUS")){
				Authority auth;
				principal = LoginService.getPrincipal();

				auth = new Authority();
				auth.setAuthority(Authority.ADMINISTRATOR);
				Assert.isTrue(principal.getAuthorities().contains(auth));
				}
			Md5PasswordEncoder encoder;
			
			encoder = new Md5PasswordEncoder();
			ranger.getUserAccount().setPassword(encoder.encodePassword(ranger.getUserAccount().getPassword(),null));
			ranger.getUserAccount().setAccountNonLocked(true);
		}

		result = rangerRepository.save(ranger);

		if (ranger.getId() == 0) {
			result = (Ranger)actorService.addSystemFolders(result);
			
		} else{
			result.setSuspicious(configurationService.hasSpam((Actor) result));
		}

		result = rangerRepository.save(result);
		
		return result;
	}



	// Other business methods ---------------------------------------------------
	
	
}