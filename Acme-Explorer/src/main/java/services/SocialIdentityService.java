
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.SocialIdentityRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.SocialIdentity;

@Service
@Transactional
public class SocialIdentityService {

	// Managed repository ------------------------------------
	
	@Autowired
	private SocialIdentityRepository	socialIdentityRepository;
	
	@Autowired
	private ActorService	actorService;

	// Constructors --------------------------------------------
	
	public SocialIdentityService() {
		super();
	}
	
	// Simple CRUD methods -------------------------------------
	
	public SocialIdentity create(int actorId) {
		Actor actor;
		SocialIdentity result;

		actor = actorService.findOne(actorId);
		result = new SocialIdentity();
		result.setActor(actor);
		
		return result;
	}

	public Collection<SocialIdentity> findAll() {
		Collection<SocialIdentity> result;

		result = this.socialIdentityRepository.findAll();
		Assert.notNull(result);

		return result;
	}
	
	public SocialIdentity findOne(int socialIdentityId) {
		Assert.isTrue(socialIdentityId != 0);
		
		SocialIdentity result;

		result = this.socialIdentityRepository.findOne(socialIdentityId);

		return result;
	}

	public SocialIdentity save(SocialIdentity socialIdentity) {
		Assert.notNull(socialIdentity);
		
		SocialIdentity result;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();

		Assert.isTrue(socialIdentity.getActor().getUserAccount().equals(principal));
		
		result = this.socialIdentityRepository.save(socialIdentity);
		
		return result;
	}

	public void delete(SocialIdentity socialIdentity) {
		Assert.notNull(socialIdentity);
		UserAccount principal;
		
		principal = LoginService.getPrincipal();

		Assert.isTrue(socialIdentity.getActor().getUserAccount().equals(principal));

		this.socialIdentityRepository.delete(socialIdentity);
	}

	//Other business methods -----------------------------------
	
	
}
