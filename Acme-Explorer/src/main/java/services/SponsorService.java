package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.SponsorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Folder;
import domain.SocialIdentity;
import domain.Sponsor;
import domain.Sponsorship;

@Service
@Transactional
public class SponsorService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private SponsorRepository sponsorRepository;

	// Supporting services ---------------------------------------------------
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private ActorService actorService;
	
	// Constructor ---------------------------------------------------

	public SponsorService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	
	public Sponsor create() {
		
		Sponsor result;
		
		result = new Sponsor();
		
		result.setSponsorships(new ArrayList<Sponsorship>());
		result.setSocialIdentities(new ArrayList<SocialIdentity>());
		result.setFolders(new ArrayList<Folder>());
		result.setBanned(false);
		result.setSuspicious(false);
		
		return result;
	}
	
	public Sponsor findOne(int sponsorId) {
		Assert.isTrue(sponsorId != 0);

		Sponsor result;

		result = sponsorRepository.findOne(sponsorId);

		return result;
	}

	public Collection<Sponsor> findAll() {
		Collection<Sponsor> result;

		result = sponsorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Sponsor save(Sponsor sponsor) {
		Assert.notNull(sponsor);
		
		UserAccount principal;
		Sponsor result;
		
		principal = LoginService.getPrincipal();
		
		if(sponsor.getId() != 0){
			Assert.isTrue(sponsor.getUserAccount().equals(principal));
		}else{
			Authority auth;
			Md5PasswordEncoder encoder;
			
			auth = new Authority();
			auth.setAuthority(Authority.ADMINISTRATOR);
			Assert.isTrue(principal.getAuthorities().contains(auth));
			encoder = new Md5PasswordEncoder();
			sponsor.getUserAccount().setPassword(encoder.encodePassword(sponsor.getUserAccount().getPassword(),null));
			sponsor.getUserAccount().setAccountNonLocked(true);

		}
		
		result = sponsorRepository.save(sponsor);
		
		if (sponsor.getId() == 0) {
			result = (Sponsor)actorService.addSystemFolders(result);

		} else{
			result.setSuspicious(configurationService.hasSpam((Actor) result));
		}
		
		result = sponsorRepository.save(result);

		return result;
	}

	// Other business methods ---------------------------------------------------
	
	
}
