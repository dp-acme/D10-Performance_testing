package converters;


import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.EmergencyContact;

@Component
@Transactional
public class EmergencyContactToStringConverter implements Converter<EmergencyContact, String>{

	@Override
	public String convert(EmergencyContact source) {
		String result;
		if(source == null)
			result = null;
		else
			result = String.valueOf(source.getId());
		
		return result;
	}

}
