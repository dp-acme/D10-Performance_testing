package converters;

import java.net.URLEncoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.CreditCard;

@Component
@Transactional
public class CreditcardToStringConverter implements Converter<CreditCard, String>{

	@Override
	public String convert(CreditCard source) {
		String result; 
		StringBuilder builder;
		
		if(source == null){
			result = null;
		}else{
			try{
				builder = new StringBuilder();
				builder.append(URLEncoder.encode(source.getHolderName(), "UTF-8"));
				builder.append("|");
				builder.append(URLEncoder.encode(source.getBrandName(), "UTF-8"));
				builder.append("|");
				builder.append(URLEncoder.encode(source.getNumber(), "UTF-8"));
				builder.append("|");
				builder.append(URLEncoder.encode(Integer.toString(source.getExpirationMonth()), "UTF-8"));
				builder.append("|");
				builder.append(URLEncoder.encode(Integer.toString(source.getExpirationYear()), "UTF-8"));
				builder.append("|");
				builder.append(URLEncoder.encode(Integer.toString(source.getCvv()), "UTF-8"));
				builder.append("|");
				result = builder.toString();
			}catch(Throwable oops){
				throw new IllegalArgumentException(oops);
			}
		}
		return result;
	}

}
