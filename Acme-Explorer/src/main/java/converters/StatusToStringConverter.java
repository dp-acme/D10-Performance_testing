package converters;

import java.net.URLEncoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Status;

@Component
@Transactional
public class StatusToStringConverter implements Converter<Status, String>{

	@Override
	public String convert(Status source) {
		String result; 
		StringBuilder builder;
		
		if(source == null){
			result = null;
		}else{
			try{
				builder = new StringBuilder();
				builder.append(URLEncoder.encode(source.getStatus(), "UTF-8"));
				result = builder.toString();
			}catch(Throwable oops){
				throw new IllegalArgumentException(oops);
			}
		}
		return result;
	}

}
