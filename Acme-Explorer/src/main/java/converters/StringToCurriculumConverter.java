package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.CurriculumRepository;
import domain.Curriculum;

@Component
@Transactional
public class StringToCurriculumConverter implements Converter<String, Curriculum>{

	@Autowired CurriculumRepository curriculumRepository;
	
	@Override
	public Curriculum convert(String source) {
		Curriculum result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = curriculumRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}