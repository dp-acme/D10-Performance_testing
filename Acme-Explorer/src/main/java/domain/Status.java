package domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Embeddable
@Access(AccessType.PROPERTY)
public class Status {

	// Constructors -----------------------------------------------------------

	public Status() {
		super();
	}

	// Values -----------------------------------------------------------------

	public static final String PENDING = "PENDING";
	public static final String REJECTED = "REJECTED";
	public static final String DUE = "DUE";
	public static final String ACCEPTED = "ACCEPTED";
	public static final String CANCELLED = "CANCELLED";

	// Attributes -------------------------------------------------------------

	private String status;

	@NotBlank
	@Pattern(regexp = "^" + Status.PENDING + "|" + Status.REJECTED + "|"
			+ Status.DUE + "|" + Status.ACCEPTED + "|" + Status.CANCELLED + "$")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}
	
	
	public static Collection<Status> listStatus() {
		Collection<Status> result;
		Status status;

		result = new ArrayList<Status>();

		status = new Status();
		status.setStatus(Status.PENDING);
		result.add(status);

		status = new Status();
		status.setStatus(Status.REJECTED);
		result.add(status);

		status = new Status();
		status.setStatus(Status.DUE);
		result.add(status);

		status = new Status();
		status.setStatus(Status.ACCEPTED);
		result.add(status);

		status = new Status();
		status.setStatus(Status.CANCELLED);
		result.add(status);

		return result;
	}
}