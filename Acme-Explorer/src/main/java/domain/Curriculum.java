package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

@Entity
@Access(AccessType.PROPERTY)
public class Curriculum extends DomainEntity {

	// Constructor
	
	public Curriculum() {
		super();
	}
	
	// Attributes
	
	private String ticker;
	private String name;
	private String email;
	private String photo;
	private String phone;
	private String profile;
	
	@Column(unique = true)
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	
	@NotBlank
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	@Email
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@NotBlank
	@URL
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	@NotBlank
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@NotBlank
	@URL
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	// Relationships
	
	private Ranger ranger;
	private Collection<Education> educationRecords;
	private Collection<Endorser> endorserRecords;
	private Collection<Miscellaneous> miscellaneousRecords;
	private Collection<Professional> professionalRecords;

	@NotNull
	@Valid
	@OneToOne(optional=false)
	public Ranger getRanger() {
		return ranger;
	}
	public void setRanger(Ranger ranger) {
		this.ranger = ranger;
	}
	
	@NotNull
	@Valid
	@OneToMany(mappedBy="curriculum")
	public Collection<Education> getEducationRecords() {
		return educationRecords;
	}
	public void setEducationRecords(Collection<Education> educationRecords) {
		this.educationRecords = educationRecords;
	}

	@NotNull
	@Valid
	@OneToMany(mappedBy="curriculum")
	public Collection<Endorser> getEndorserRecords() {
		return endorserRecords;
	}
	public void setEndorserRecords(Collection<Endorser> endorserRecords) {
		this.endorserRecords = endorserRecords;
	}

	@NotNull
	@Valid
	@OneToMany(mappedBy="curriculum")
	public Collection<Miscellaneous> getMiscellaneousRecords() {
		return miscellaneousRecords;
	}
	public void setMiscellaneousRecords(
			Collection<Miscellaneous> miscellaneousRecords) {
		this.miscellaneousRecords = miscellaneousRecords;
	}

	@NotNull
	@Valid
	@OneToMany(mappedBy="curriculum")
	public Collection<Professional> getProfessionalRecords() {
		return professionalRecords;
	}
	public void setProfessionalRecords(Collection<Professional> professionalRecords) {
		this.professionalRecords = professionalRecords;
	}	
}
