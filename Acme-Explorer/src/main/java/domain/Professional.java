package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)

public class Professional extends Record {

	// Constructor
	
	public Professional() {
		super();
	}
	
	// Attributes
	
	private String companyName;
	private Date professionalStart;
	private Date professionalEnd;
	private String role;
	private String attachment;
	private String comments;
	
	@NotBlank
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getProfessionalStart() {
		return professionalStart;
	}
	public void setProfessionalStart(Date professionalStart) {
		this.professionalStart = professionalStart;
	}
	
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getProfessionalEnd() {
		return professionalEnd;
	}
	public void setProfessionalEnd(Date professionalEnd) {
		this.professionalEnd = professionalEnd;
	}
	
	@NotBlank
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	@URL
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}	
	
	// Relationships
	
	private Curriculum curriculum;

	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Curriculum getCurriculum() {
		return curriculum;
	}
	public void setCurriculum(Curriculum curriculum) {
		this.curriculum = curriculum;
	}	
}
