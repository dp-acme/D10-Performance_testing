package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Administrator;
import domain.Manager;
import domain.Ranger;
import domain.Trip;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer>{

	//*****DASHBOARD*******
	
	@Query("select avg(cast((select count (*) from ApplyFor ap where ap.trip = t) as float)), " +
			"max(cast((select count (*) from ApplyFor ap where ap.trip = t) as float)), " +
			"min(cast((select count (*) from ApplyFor ap where ap.trip = t) as float)), " +
			"sqrt(sum((select count (*) from ApplyFor ap where ap.trip = t)*(select count (*) from ApplyFor ap where ap.trip = t))/(select count (*) from Trip t2) - (avg(cast((select count (*) from ApplyFor ap where ap.trip = t) as float)))*(avg(cast((select count (*) from ApplyFor ap where ap.trip = t) as float)))) " +
			"from Trip t")
	Double[] getAvgMaxMinSdApplicationOfTrips();
	
	@Query("select avg(m.trips.size), max(m.trips.size), min(m.trips.size)," +
			"sqrt(sum(m.trips.size*m.trips.size)/count(m.trips.size)-avg(m.trips.size)*avg(m.trips.size)) " +
			"from Manager m")
	Double[] getAvgMaxMinSdManagersOfTrips();
	
	@Query("select avg(t.price), max(t.price), min(t.price)," +
			"sqrt(sum(t.price*t.price)/count(t.price)-avg(t.price)*avg(t.price)) " +
			"from Trip t")
	Double[] getAvgMaxMinSdPriceOfTrips();
	
	@Query("select avg(cast((select count(t) from Trip t where t.ranger = r2) as float)), " +
			"max(cast((select count(t) from Trip t where t.ranger = r2) as int)), " +
			"min(cast((select count(t) from Trip t where t.ranger = r2) as int)), " +
			"sqrt(sum((select count(t) from Trip t where t.ranger = r2)*(select count(t) from Trip t where t.ranger = r2)) / (select count(r3) from Ranger r3) - avg(cast((select count(t) from Trip t where t.ranger = r2) as float))*avg(cast((select count(t) from Trip t where t.ranger = r2) as float))) " +
			"from Ranger r2")
	Double[] getAvgMaxMinSdRangersOfTrips();
	

	@Query("select (select count(*) from ApplyFor a where a.status='PENDING')/cast(count(*) as float) from ApplyFor a")
	Double getRatioOfPendingApplications();
	
	@Query("select (select count(*) from ApplyFor a where a.status='DUE')/cast(count(*) as float) from ApplyFor a")
	Double getRatioOfDueApplications();
	
	@Query("select (select count(*) from ApplyFor a where a.status='ACCEPTED')/cast(count(*) as float) from ApplyFor a")
	Double getRatioOfAcceptedApplications();
	
	@Query("select (select count(*) from ApplyFor a where a.status='CANCELLED')/cast(count(*) as float) from ApplyFor a")
	Double getRatioOfCancelledApplications();
	

	@Query("select cast(count(t.cancellationReason) as float)/(select count(*) from Trip) from Trip t")
	Double getRatioOfCancelledTrips();
	
	@Query("select t2 from Trip t2 where(select count (*) from ApplyFor ap where ap.trip = t2) > (select avg(cast((select count (*) " +
			"from ApplyFor ap where ap.trip = t1) as float)) * 1.1 from Trip t1)) " +
			"order by cast((select count (*) from ApplyFor ap where ap.trip = t) as float) asc")
	Collection<Trip> getTripsOver10PercentOverAvgApplications();
	
	@Query("select lt, (select count(t) from Trip t where t.legalText = lt) from LegalText lt where lt.finalMode = true")	
	Collection<Object[]> getLegalTextNumberOfTrips();
	
	
	@Query("select avg(cast((select count (*) from Note n where n.trip = t) as float)), " +
			"max(cast((select count (*) from Note n where n.trip = t) as float)), " +
			"min(cast((select count (*) from Note n where n.trip = t) as float)), " +
			"sqrt(sum((select count (*) from Note n where n.trip = t)*(select count (*) from Note n where n.trip = t))/(select count (*) from Trip t2) - (avg(cast((select count (*) from Note n where n.trip = t) as float)))*(avg(cast((select count (*) from Note n where n.trip = t) as float)))) " +
			"from Trip t")
	Double[] getAvgMaxMinSdNotesOfTrips();
	

	@Query("select avg(cast((select count (*) from AuditRecord a where a.trip = t) as float)), " +
			"max(cast((select count (*) from AuditRecord a where a.trip = t) as float)), " +
			"min(cast((select count (*) from AuditRecord a where a.trip = t) as float)), " +
			"sqrt(sum((select count (*) from AuditRecord a where a.trip = t)*(select count (*) from AuditRecord a where a.trip = t))/(select count (*) from Trip t2) - (avg(cast((select count (*) from AuditRecord a where a.trip = t) as float)))*(avg(cast((select count (*) from AuditRecord a where a.trip = t) as float)))) " +
			"from Trip t")
	Double[] getAvgMaxMinSdAuditRecordsOfTrips();
	
	
	@Query("select cast(count(t) as float) / (select count(t2) from Trip t2) from Trip t where (select count (*) from AuditRecord a where a.trip = t) > 0")
	Double getRatioOfTripsWithAuditRecords();
		

	@Query("select (select count(r) from Ranger r where r.curriculum is not null)/cast(count(r) as float) from Ranger r")
	Double getRatioOfRangersWithCurriculum();	

	@Query("select (select count(r) from Ranger r where r.curriculum is not null and r.curriculum.endorserRecords.size > 0)/cast((select count(r2) from Ranger r2) as float) from Curriculum c join c.endorserRecords where c.ranger > 0")
	Double getRatioOfRangersWithEndorsedCurriculum();
	

	@Query("select cast(count(ms) as float)/ (select count(m) from Manager m) from Manager ms where ms.suspicious = true")
	Double getRatioOfSuspiciousManagers();
	
	@Query("select cast(count(rs) as float)/ (select count(r) from Ranger r) from Ranger rs where rs.suspicious = true")
	Double getRatioOfSuspiciousRangers();

	//Otras consultas
	@Query("select r from Ranger r where r.suspicious = true")
	Collection<Ranger> getSuspiciousRangers();
	
	@Query("select m from Manager m where m.suspicious = true")
	Collection<Manager> getSuspiciousManagers();
	
	@Query("select a from Actor a where a.suspicious = true")
	Collection<Actor> getSuspiciousActors();
}
