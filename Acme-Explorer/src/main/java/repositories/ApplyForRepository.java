package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.ApplyFor;
import domain.Status;

@Repository
public interface ApplyForRepository extends JpaRepository<ApplyFor, Integer>{
	
	@Query("select ap from ApplyFor ap where ap.explorer.id = ?1 AND ap.status = ?2")
	Collection<ApplyFor> getApplicationsFromExplorerAndStatus(int explorerId, Status status);
	
	@Query("select ap from ApplyFor ap where ap.explorer.id = ?1 AND ap.trip.id = ?2")
	ApplyFor getApplicationFromExplorerAndTrip(int explorerId, int tripId);
	
	@Query("select ap from ApplyFor ap where ap.status = ?1")
	Collection<ApplyFor> filterApplicationsByStatus(Status status);
	
	@Query("select ap from ApplyFor ap where ap.status <> ?1")
	Collection<ApplyFor> filterApplicationsByDifferentStatus(Status status);
	
	@Query("select ap from ApplyFor ap where ap.trip.id = ?1")
	Collection<ApplyFor> getApplicationsByTrip(int tripId);
	
}
