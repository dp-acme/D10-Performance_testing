package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import domain.Education;

@Repository
public interface EducationRepository extends JpaRepository<Education, Integer>{

}
