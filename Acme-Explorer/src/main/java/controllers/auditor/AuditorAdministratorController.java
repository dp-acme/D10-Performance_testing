/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.auditor;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.AuditorService;
import services.ConfigurationService;
import domain.Auditor;

@Controller
@RequestMapping("/auditor/administrator")
public class AuditorAdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private AuditorService auditorService;
	

	@Autowired
	private ConfigurationService configurationService;
	
	// Constructors -----------------------------------------------------------

	public AuditorAdministratorController() {
		super();
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		Auditor auditor;
		
		auditor = this.auditorService.create();
		result = this.createEditModelAndView(auditor);

		
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView saveCreate(@Valid Auditor auditor, BindingResult binding){
		ModelAndView result;

		if(binding.hasErrors()){
			result = createEditModelAndView(auditor);
		} else { 
			try {
				auditorService.save(auditor);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(auditor, "auditor.commit.error");
			}
		}
		return result;
	}
	
	
	protected ModelAndView createEditModelAndView(Auditor auditor) {
		ModelAndView result; 
		
		result = createEditModelAndView(auditor, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Auditor auditor,
			String messageCode) {
		ModelAndView result;

		result = new ModelAndView("auditor/administrator/create");
		result.addObject("auditor", auditor);
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());

		result.addObject("message", messageCode);
		
		return result;
	}

}
