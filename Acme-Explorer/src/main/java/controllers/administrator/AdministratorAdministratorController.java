/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AdministratorService;
import services.ConfigurationService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.Manager;
import domain.Ranger;

@Controller
@RequestMapping("/administrator/administrator")
public class AdministratorAdministratorController extends AbstractController {

	@Autowired
	private AdministratorService administratorService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private ConfigurationService configurationService;
	
	// Constructors -----------------------------------------------------------

	public AdministratorAdministratorController() {
		super();
	}
	
	@RequestMapping(value="/suspicious", method = RequestMethod.GET)
	public ModelAndView suspicious(){
		ModelAndView result;
		Collection<Manager> managers;
		Collection<Ranger> rangers;
		
		managers = administratorService.getSuspiciousManagers();
		rangers = administratorService.getSuspiciousRangers();
		
		result = new ModelAndView("administrator/administrator/suspicious");
		
		result.addObject("managers", managers);
		result.addObject("rangers", rangers);
		
		return result;
	}
	
	@RequestMapping(value="/ban", method = RequestMethod.GET)
	public ModelAndView ban(@RequestParam(value = "actorId", required = true) Integer actorId){
		ModelAndView result;

		result = new ModelAndView("redirect:/administrator/administrator/suspicious.do");

		try{
			administratorService.banActor(actorId);
			
		} catch (Throwable oops) {
			result.addObject("message", "ERRO");
		}
		
		return result;
	}
	
	@RequestMapping(value="/unban", method = RequestMethod.GET)
	public ModelAndView unban(@RequestParam(value = "actorId", required = true) Integer actorId){
		ModelAndView result;

		result = new ModelAndView("redirect:/administrator/administrator/suspicious.do");

		try{
			administratorService.unbanActor(actorId);
			
		} catch (Throwable oops) {
			result.addObject("message", "ERRO");
		}
		
		return result;
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public ModelAndView edit(){
		ModelAndView result;
		Actor principal;
		Administrator administrator;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Administrator);
		administrator = (Administrator) principal;
		Assert.notNull(administrator);
		result = this.createEditModelAndView(administrator);
		
		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@Valid Administrator administrator, BindingResult binding){
		ModelAndView result;
		if(binding.hasErrors()){
			result = createEditModelAndView(administrator);
		} else { 
			try {
				administratorService.save(administrator);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(administrator, "administrator.commit.error");
			}
		}
		return result;
	}
	
	protected ModelAndView createEditModelAndView(Administrator administrator) {
		ModelAndView result; 
		
		result = createEditModelAndView(administrator, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Administrator administrator,
			String messageCode) {
		ModelAndView result;
			
		result = new ModelAndView("administrator/administrator/edit");
		result.addObject("administrator", administrator);
		result.addObject("actorId", administrator.getId());
		result.addObject("socialIdentities", administrator.getSocialIdentities());
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());

		result.addObject("message", messageCode);
		
		return result;
	}
	
}
