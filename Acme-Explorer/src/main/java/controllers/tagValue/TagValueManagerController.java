package controllers.tagValue;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.TagKeyService;
import services.TagValueService;
import services.TripService;
import controllers.AbstractController;
import domain.Actor;
import domain.Manager;
import domain.TagKey;
import domain.TagValue;
import domain.Trip;

@Controller
@RequestMapping("/tagvalue/manager")
public class TagValueManagerController extends AbstractController {
	
	// Services------------------------------
		@Autowired
		private TagValueService tagValueService;
		
		@Autowired
		private TripService tripService;
		
		@Autowired
		private ActorService actorService;
		
		@Autowired
		private TagKeyService tagKeyService;
	// Constructor---------------------------
		public TagValueManagerController() {
			super();
		}
	//Listing--------------------------------
		@RequestMapping(value = "/list")
		public ModelAndView list(@RequestParam("tripId") Integer tripId){
			ModelAndView result;
			Collection<TagValue> tagValues;
			Trip trip;
			
			UserAccount userAccount;
			Actor actor;

			userAccount = LoginService.getPrincipal();
			actor = actorService.findByUserAccountId(userAccount.getId());
			Assert.isInstanceOf(Manager.class, actor);
			
			trip = tripService.findOne(tripId);
			tagValues = trip.getTags();
			result = new ModelAndView("tagvalue/list");
			
			Assert.notNull(trip);
			Assert.isTrue(!(trip.getPublicationDate().after(new Date()) && !trip.getManager().equals(actor)));
			
			result.addObject("tagValues", tagValues);
			result.addObject("trip", trip);
			result.addObject("requestUri", "tagvalue/manager/list.do");
			
			return result;
		}
		
		@RequestMapping(value = "/create", method=RequestMethod.GET)
		public ModelAndView create(@RequestParam("tripId") int tripId){
			ModelAndView result;
			TagValue tagValue;
			
			Assert.notNull(tripService.findOne(tripId));
			
			tagValue = this.tagValueService.create();

			result = this.createEditModelAndView(tagValue);
			result.addObject("tripId", tripId);
			result.addObject("requestUri", "tagvalue/manager/create.do");
			
			return result;
		}
		
		@RequestMapping(value = "/create", method=RequestMethod.POST, params="save")
		public ModelAndView save(@Valid TagValue tagValue, BindingResult binding, @RequestParam("tripId") int tripId){
			ModelAndView result;
			Trip trip;
			Collection<TagValue> tagValues;
			
			
			if(binding.hasErrors()){
				result = createEditModelAndView(tagValue);
				result.addObject("tripId", tripId);
				
			}else{
				try{
					tagValue = tagValueService.save(tagValue);
					trip = tripService.findOne(tripId);		
					
					Assert.notNull(trip);
					
					tagValues = trip.getTags();
					tagValues.add(tagValue);
					trip.setTags(tagValues);
					tripService.save(trip);
					
					result = new ModelAndView("redirect:list.do?tripId=" + tripId);
					
				}catch(Throwable oops){					
					result = createEditModelAndView(tagValue, "tagValue.commit.error");
					result.addObject("tripId", tripId);
				}
			}
			
			return result;
		}
		
		@RequestMapping(value = "/delete", method=RequestMethod.GET)
		public ModelAndView delete(@RequestParam("tagvalueId") int tagValueId, @RequestParam("tripId") int tripId){
			ModelAndView result;
			TagValue tagValue;
			Trip trip;
			
			trip = tripService.findOne(tripId);
			tagValue = tagValueService.findOne(tagValueId);
			tagValueService.delete(tagValue);
			
			result = new ModelAndView("redirect:list.do?tripId="+trip.getId());
		
			
			return result;
		}
		
		protected ModelAndView createEditModelAndView(TagValue tagValue){
			ModelAndView result;
			
			result = createEditModelAndView(tagValue, null);
			
			return result;
		}
		
		protected ModelAndView createEditModelAndView(TagValue tagValue, String messageCode){
			ModelAndView result;
			Collection<TagKey> tagKeys;
			
			tagKeys = tagKeyService.findAll();
			
			result = new ModelAndView("tagvalue/manager/create");
			result.addObject("tagKeys", tagKeys);
			result.addObject("tagValue", tagValue);
			result.addObject("message", messageCode);
			
			
			return result;
	 	}
			

}
