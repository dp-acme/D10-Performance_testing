/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;
import services.ConfigurationService;
import controllers.AbstractController;

@Controller
@RequestMapping("/dashboard")
public class DashboardController extends AbstractController {

	// Services -----------------------------------------------------------
	
	@Autowired
	AdministratorService administratorService;
	
	@Autowired
	ConfigurationService configurationService;
	
	// Constructors -----------------------------------------------------------

	public DashboardController() {
		super();
	}

	@RequestMapping("/display")
	public ModelAndView display() {
		String[] avgMaxMinSdApplicationOfTrips, avgMaxMinSdManagersOfTrips,
			     avgMaxMinSdPriceOfTrips, avgMaxMinSdRangersOfTrips,
			     avgMaxMinSdAuditRecordsOfTrips, avgMaxMinSdNotesOfTrips;
		
		String ratioOfPendingApplications, ratioOfDueApplications,
		       ratioOfAcceptedApplications, ratioOfCancelledApplications;
		
		String ratioOfCancelledTrips, ratioOfTripsWithAuditRecords;
		String ratioOfRangersWithCurriculum, ratioOfRangersWithEndorsedCurriculum;
		String ratioOfSuspiciousManagers, ratioOfSuspiciousRangers;
		
		ModelAndView result;

		result = new ModelAndView("dashboard/display");
		
		avgMaxMinSdApplicationOfTrips = configurationService.roundTo(administratorService.getAvgMaxMinSdApplicationOfTrips(), 2);
		avgMaxMinSdManagersOfTrips = configurationService.roundTo(administratorService.getAvgMaxMinSdManagersOfTrips(), 2);
		avgMaxMinSdPriceOfTrips = configurationService.roundTo(administratorService.getAvgMaxMinSdPriceOfTrips(), 2);
		avgMaxMinSdRangersOfTrips = configurationService.roundTo(administratorService.getAvgMaxMinSdRangersOfTrips(), 2);
		avgMaxMinSdAuditRecordsOfTrips = configurationService.roundTo(administratorService.getAvgMaxMinSdAuditRecordsOfTrips(), 2);
		avgMaxMinSdNotesOfTrips = configurationService.roundTo(administratorService.getAvgMaxMinSdNotesOfTrips(), 2);
		
		ratioOfPendingApplications = configurationService.roundTo(administratorService.getRatioOfPendingApplications(), 2);
		ratioOfDueApplications = configurationService.roundTo(administratorService.getRatioOfPendingApplications(), 2);
		ratioOfAcceptedApplications = configurationService.roundTo(administratorService.getRatioOfAcceptedApplications(), 2);
		ratioOfCancelledApplications = configurationService.roundTo(administratorService.getRatioOfCancelledApplications(), 2);
		
		ratioOfCancelledTrips = configurationService.roundTo(administratorService.getRatioOfCancelledTrips(), 2);
		ratioOfTripsWithAuditRecords = configurationService.roundTo(administratorService.getRatioOfTripsWithAuditRecords(), 2);
		
		ratioOfRangersWithCurriculum = configurationService.roundTo(administratorService.getRatioOfRangersWithCurriculum(), 2);
		ratioOfRangersWithEndorsedCurriculum = configurationService.roundTo(administratorService.getRatioOfRangersWithEndorsedCurriculum(), 2);
		
		ratioOfSuspiciousManagers = configurationService.roundTo(administratorService.getRatioOfSuspiciousManagers(), 2);
		ratioOfSuspiciousRangers = configurationService.roundTo(administratorService.getRatioOfSuspiciousRangers(), 2);
		
		result.addObject("avgMaxMinSdApplicationOfTrips", avgMaxMinSdApplicationOfTrips);
		result.addObject("avgMaxMinSdManagersOfTrips", avgMaxMinSdManagersOfTrips);
		result.addObject("avgMaxMinSdPriceOfTrips", avgMaxMinSdPriceOfTrips);
		result.addObject("avgMaxMinSdRangersOfTrips", avgMaxMinSdRangersOfTrips);
		result.addObject("avgMaxMinSdAuditRecordsOfTrips", avgMaxMinSdAuditRecordsOfTrips);
		result.addObject("avgMaxMinSdNotesOfTrips", avgMaxMinSdNotesOfTrips);
		
		result.addObject("ratioOfPendingApplications", ratioOfPendingApplications);
		result.addObject("ratioOfDueApplications", ratioOfDueApplications);
		result.addObject("ratioOfAcceptedApplications", ratioOfAcceptedApplications);
		result.addObject("ratioOfCancelledApplications", ratioOfCancelledApplications);
		
		result.addObject("ratioOfCancelledTrips", ratioOfCancelledTrips);
		result.addObject("tripsOver10PercentOverAvgApplications", administratorService.getTripsOver10PercentOverAvgApplications());
		result.addObject("ratioOfTripsWithAuditRecords", ratioOfTripsWithAuditRecords);

		result.addObject("ratioOfRangersWithCurriculum", ratioOfRangersWithCurriculum);
		result.addObject("ratioOfRangersWithEndorsedCurriculum", ratioOfRangersWithEndorsedCurriculum);

		result.addObject("ratioOfSuspiciousManagers", ratioOfSuspiciousManagers);
		result.addObject("ratioOfSuspiciousRangers", ratioOfSuspiciousRangers);

		result.addObject("legalTextNumberOfTrips", administratorService.getLegalTextNumberOfTrips());
		
		return result;
	}

}