/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.legalText;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.LegalTextService;
import controllers.AbstractController;
import domain.LegalText;

@Controller
@RequestMapping("/legalText/administrator")
public class LegalTextAdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private LegalTextService legalTextService;
	
	// Constructors -----------------------------------------------------------

	public LegalTextAdministratorController() {
		super();
	}

	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		LegalText legalText;
		
		legalText = this.legalTextService.create();
		result = this.createEditModelAndView(legalText);
		
		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int legalTextId){
		ModelAndView result;
		LegalText legalText;
		
		legalText = legalTextService.findOne(legalTextId);
		Assert.notNull(legalText);
		result = createEditModelAndView(legalText);

		
		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@Valid LegalText legalText, BindingResult binding){
		ModelAndView result;
	
		if(binding.hasErrors()){
			result = createEditModelAndView(legalText);
		} else { 
			try {
				legalTextService.editAttributes(legalText);
				result = new ModelAndView("redirect:/legalText/list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(legalText, "legalText.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="delete")
	public ModelAndView delete(@Valid LegalText legalText, BindingResult binding){
		ModelAndView result;
	
			try {
				legalTextService.delete(legalText);
				result = new ModelAndView("redirect:/legalText/list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(legalText, "legalText.commit.error");
			}
		return result;
	}

	protected ModelAndView createEditModelAndView(LegalText legalText) {
		ModelAndView result; 
		
		result = createEditModelAndView(legalText, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(LegalText legalText,
			String messageCode) {
		ModelAndView result;
		if(legalText.getId()==0){
			result = new ModelAndView("legalText/administrator/create");
		}else{
			result = new ModelAndView("legalText/administrator/edit");
		}
			result.addObject("legalText", legalText);
		
		result.addObject("message", messageCode);
		
		return result;
	}

}
