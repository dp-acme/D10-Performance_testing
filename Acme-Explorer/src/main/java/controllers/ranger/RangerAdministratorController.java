/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.ranger;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.ConfigurationService;
import services.RangerService;
import domain.Ranger;

@Controller
@RequestMapping("/ranger/administrator")
public class RangerAdministratorController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private RangerService rangerService;
	

	@Autowired
	private ConfigurationService configurationService;
	
	// Constructors -----------------------------------------------------------

	public RangerAdministratorController() {
		super();
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		Ranger ranger;
		
		ranger = this.rangerService.create();
		result = this.createEditModelAndView(ranger);

		
		return result;
	}
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView saveCreate(@Valid Ranger ranger, BindingResult binding){
		ModelAndView result;
		if(binding.hasErrors()){
			result = createEditModelAndView(ranger);
		} else { 
			try {
				rangerService.save(ranger);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(ranger, "ranger.commit.error");
			}
		}
		return result;
	}
	
	protected ModelAndView createEditModelAndView(Ranger ranger) {
		ModelAndView result; 
		
		result = createEditModelAndView(ranger, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Ranger ranger,
			String messageCode) {
		ModelAndView result;
		result = new ModelAndView("ranger/administrator/create");
		result.addObject("ranger", ranger);
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());
		
		result.addObject("message", messageCode);
		
		return result;
	}

}
