
package controllers.curriculum;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CurriculumService;
import services.ProfessionalService;
import controllers.AbstractController;
import domain.Curriculum;
import domain.Professional;

@Controller
@RequestMapping("/professional")
public class ProfessionalController extends AbstractController {

	//Services------------------------------------------------------------
	@Autowired
	private ProfessionalService	professionalService;

	@Autowired
	private CurriculumService	curriculumService;

	//Constructors------------------------------------------------------------
	private ProfessionalController() {
		super();
	}

	//Display-------------------------------------------------------------------------------
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam final int professionalId) {
		ModelAndView result;
		Professional professional;

		professional = this.professionalService.findOne(professionalId);
		Assert.notNull(professional);

		result = new ModelAndView("professional/display");
		result.addObject("professional", professional);
		result.addObject("curriculumId", professional.getCurriculum().getId());

		return result;
	}
	//List -------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam int curriculumId) {
		ModelAndView result;
		Collection<Professional> professionalRecords;
		Curriculum curriculum;
		curriculum = this.curriculumService.findOne(curriculumId);
	
		professionalRecords = curriculum.getProfessionalRecords();

		result = new ModelAndView("professional/list");
		result.addObject("professionalRecords", professionalRecords);

		return result;
	}
}
