
package controllers.curriculum;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.CurriculumService;
import services.EndorserService;
import domain.Curriculum;
import domain.Endorser;

@Controller
@RequestMapping("/endorser")
public class EndorserController extends AbstractController {

	//Services --------------------------------------------------
	@Autowired
	private EndorserService	endorserService;

	@Autowired
	private CurriculumService	curriculumService;
	//Constructors --------------------------------------------------
	public EndorserController() {
		super();
	}

	//Display --------------------------------------------------
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam int endorserId) {
		ModelAndView result;
		Endorser endorser;

		endorser = this.endorserService.findOne(endorserId);
		Assert.notNull(endorser);

		result = new ModelAndView("endorser/display");
		result.addObject("endorser", endorser);
		result.addObject("curriculumId", endorser.getCurriculum().getId());

		return result;
	}
	//List -------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam int curriculumId) {
		ModelAndView result;
		Collection<Endorser> endorserRecords;
			Curriculum curriculum;
			curriculum = this.curriculumService.findOne(curriculumId);
		
			endorserRecords = curriculum.getEndorserRecords();

		result = new ModelAndView("endorser/list");
		result.addObject("endorserRecords", endorserRecords);

		return result;
	}
}
