<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<jsp:useBean id="date" class="java.util.Date" />

<%-- <jstl:set var="uri" value="trip/list.do" /> --%>
<%-- <jstl:set var="search" value="trip/list.do" /> --%>

<%-- <security:authorize access="hasRole('MANAGER')"> --%>
<%-- 	<jstl:set var="uri" value="trip/manager/list.do" /> --%>
<%-- 	<jstl:set var="search" value="trip/manager/list.do" /> --%>
<%-- </security:authorize> --%>

<%-- <security:authorize access="hasRole('EXPLORER')"> --%>
<%-- 	<jstl:set var="uri" value="trip/explorer/list.do" /> --%>
<%-- 	<jstl:set var="search" value="trip/explorer/list.do" /> --%>
<%-- </security:authorize> --%>

<%-- <form:form method="POST" action="${search}"> --%>
<%-- 	<form:label path="keyword"> --%>
<%-- 		<spring:message code="trip.search.keyword"/> --%>
<%-- 	</form:label> --%>
<%-- 	<form:input path="keyword"/> --%>

<%-- 	<spring:message code="trip.search.button" var="trip.search.button" /> --%>
<%-- 	<input type="submit" name="searchKeyword" value="${trip.search.button}" /> --%>
<%-- </form:form> --%>

<security:authorize access = "hasAnyRole('ADMINISTRATOR','RANGER','AUDITOR','SPONSOR','MANAGER') or isAnonymous()">
<form action="${search}" method="post">
	<input type="text" name="keyword">
	<spring:message code="trip.search.button" var="searchButton" />
	<input type="submit" name="search" value="${searchButton}" />
</form>
</security:authorize>


<spring:message code="trip.search.dateIntervalMessage" var="dateIntervalMessage"/>
<input type="hidden" value="${dateIntervalMessage}" id="dateIntervalMessage">

<spring:message code="trip.search.priceIntervalMessage" var="priceIntervalMessage"/>
<input type="hidden" value="${priceIntervalMessage}" id="priceIntervalMessage">

<script>
function checkPriceInterval(){
	var priceStart = document.getElementById('priceStart').value;
	var priceEnd = document.getElementById('priceEnd').value;
	
	var res = priceStart <= priceEnd;
	
	if(!res){
		alert(document.getElementById('priceIntervalMessage').value);
	}
	
	return res;
}

function checkDateFormat(str){
	var regex = /^[0-9][0-9]?\/[0-9][0-9]?\/[0-9][0-9]?[0-9]?[0-9]?$/;
	
	return regex.test(str);
}

function checkDateInterval(){
	var res = true;
	
	var dateStartStr = document.getElementById('dateStart').value;
	var dateEndStr = document.getElementById('dateEnd').value;
	
	if(checkDateFormat(dateStartStr) && checkDateFormat(dateEndStr)){
		var dateStart = new Date(dateStartStr);
		var dateEnd = new Date(dateEndStr);
		
		res = dateStart <= dateEnd;
		
		if(!res){
			alert(document.getElementById('dateIntervalMessage').value);
		}	
	}
	
	return res;
}

function checkIntervals(){
	return checkPriceInterval() && checkDateInterval();
}
</script>

<security:authorize access="hasRole('EXPLORER')">
	<jstl:if test="${requestURI=='trip/explorer/list.do'}">
		<form:form action="${requestURI}" modelAttribute="finder">
			<form:hidden path="id" />
			<form:hidden path="version" />
			
			<form:hidden path="explorer" />
			
			<fmt:formatDate value="${date}" pattern="dd/MM/yyyy HH:mm" var="formattedDate" />
			<input type="hidden" name="lastSaved" value="${formattedDate}" />
			
			<form:label path="keyWord">
				<spring:message code="trip.search.keyword" />
			</form:label>
			<form:input path="keyWord" />
			<form:errors path="keyWord" cssClass="error"/>

			<form:label path="dateStart">
				<spring:message code="trip.search.startDate" />
			</form:label>
			<form:input path="dateStart" placeholder="dd/MM/yyyy" id="dateStart" />
			<form:errors path="dateStart" cssClass="error"/>

			<form:label path="dateEnd">
				<spring:message code="trip.search.endDate" />
			</form:label>
			<form:input path="dateEnd" placeholder="dd/MM/yyyy" id="dateEnd" />
			<form:errors path="dateEnd" cssClass="error"/>

			<form:label path="priceStart">
				<spring:message code="trip.search.priceStart" />
			</form:label>
			<form:input path="priceStart" id="priceStart" />
			<form:errors path="priceStart" cssClass="error"/>

			<form:label path="priceEnd">
				<spring:message code="trip.search.priceEnd" />
			</form:label>
			<form:input path="priceEnd" id="priceEnd" />
			<form:errors path="priceEnd" cssClass="error"/>

			<spring:message code="trip.search.button" var="searchButton" />
			<input type="submit" name="search" value="${searchButton}" 
			onclick="javascript: return checkIntervals();"/>

		</form:form>
	</jstl:if>
</security:authorize>

<display:table name="trips" id="row" requestURI="${requestURI}" pagesize="5">

	<spring:message code="trip.list.tickerHeader" var="tickerHeader" />
	<display:column property="ticker" title="${tickerHeader}"
		sortable="false" />

	<spring:message code="trip.list.titleHeader" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />

	<spring:message code="trip.list.priceHeader" var="priceHeader" />
	<display:column title="${priceHeader}" sortable="true">
		<fmt:formatNumber value="${row.getPrice()}"  pattern="#,##0.00"/>
	</display:column>

	<spring:message code="trip.list.startDateHeader" var="startDateHeader" />
	<display:column title="${startDateHeader}" sortable="true">
		<spring:message code="trip.list.formatDate" var="formatDate" />
		<fmt:formatDate value="${row.getStartDate()}" pattern="${formatDate}" />
	</display:column>

	<spring:message code="trip.list.endDateHeader" var="endDateHeader" />
	<display:column title="${endDateHeader}" sortable="true">
		<spring:message code="trip.list.formatDate" var="formatDate" />
		<fmt:formatDate value="${row.getEndDate()}" pattern="${formatDate}" />
	</display:column>

	<security:authorize access="hasRole('EXPLORER')">
		<spring:message code="trip.list.displayHeader" var="displayHeader" />
		<display:column title="" sortable="false">
			<spring:url value="trip/explorer/display.do?tripId=${row.getId()}"
				var="displayTrip" />
			<a href="${displayTrip}"> <jstl:out value="${displayHeader}" />
			</a>
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('MANAGER')">
		<security:authentication property="principal.username" var="principal" />
		<jstl:choose>
			<jstl:when test="${row.getManager().getUserAccount().getUsername() == principal}">
				<spring:message code="trip.list.displayHeader" var="displayHeader" />
				<display:column title="" sortable="false">
					<spring:url value="trip/manager/display.do?tripId=${row.getId()}"
						var="displayTrip" />
					<a href="${displayTrip}" style="color: #0000E5"> <jstl:out value="${displayHeader}" />
					</a>
				</display:column>
			</jstl:when>
			
			<jstl:otherwise>
				<spring:message code="trip.list.displayHeader" var="displayHeader" />
				<display:column title="" sortable="false">
					<spring:url value="trip/manager/display.do?tripId=${row.getId()}"
						var="displayTrip" />
					<a href="${displayTrip}" style="color:red"> <jstl:out value="${displayHeader}" />
					</a>
				</display:column>
			</jstl:otherwise>
		</jstl:choose>
	</security:authorize>

	<security:authorize access="hasRole('AUDITOR')">
		<spring:message code="trip.list.displayHeader" var="displayHeader" />
		<display:column title="" sortable="false">
			<spring:url value="trip/auditor/display.do?tripId=${row.getId()}"
				var="displayTrip" />
			<a href="${displayTrip}"> <jstl:out value="${displayHeader}" />
			</a>
		</display:column>
	</security:authorize>

	<security:authorize access="hasRole('SPONSOR')">
		<spring:message code="trip.list.displayHeader" var="displayHeader" />
		<display:column title="" sortable="false">
			<spring:url value="trip/sponsor/display.do?tripId=${row.getId()}"
				var="displayTrip" />
			<a href="${displayTrip}"> <jstl:out value="${displayHeader}" />
			</a>
		</display:column>
	</security:authorize>

	<security:authorize access="hasRole('RANGER')">
		<spring:message code="trip.list.displayHeader" var="displayHeader" />
		<display:column title="" sortable="false">
			<spring:url value="trip/display.do?tripId=${row.getId()}"
				var="displayTrip" />
			<a href="${displayTrip}"> <jstl:out value="${displayHeader}" />
			</a>
		</display:column>
	</security:authorize>

	<security:authorize access="hasRole('ADMINISTRATOR')">
		<spring:message code="trip.list.displayHeader" var="displayHeader" />
		<display:column title="" sortable="false">
			<spring:url value="trip/display.do?tripId=${row.getId()}"
				var="displayTrip" />
			<a href="${displayTrip}"> <jstl:out value="${displayHeader}" />
			</a>
		</display:column>
	</security:authorize>

	<security:authorize access="isAnonymous()">
		<spring:message code="trip.list.displayHeader" var="displayHeader" />
		<display:column title="" sortable="false">
			<spring:url value="trip/display.do?tripId=${row.getId()}"
				var="displayTrip" />
			<a href="${displayTrip}"> <jstl:out value="${displayHeader}" />
			</a>
		</display:column>
	</security:authorize>

</display:table>

<security:authorize access="hasRole('MANAGER')">
	<spring:message code="trip.list.createHeader" var="createHeader" />
	<spring:url value="trip/manager/create.do" var="createTrip" />
	<a href="${createTrip}"> <jstl:out value="${createHeader}" />
	</a>
</security:authorize>