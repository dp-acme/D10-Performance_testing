<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<jsp:useBean id="time" class="java.util.Date" />

<jstl:set var="uri" value="auditrecord/list.do" />

<security:authorize access="hasRole('AUDITOR')">
	<jstl:set var="uri" value="auditrecord/auditor/list.do" />
</security:authorize>

<display:table name="auditRecords" id="row" requestURI="${uri}"
	pagesize="5">

	<spring:message code="auditRecord.list.timeHeader" var="timeHeader" />
	<display:column title="${timeHeader}" sortable="true">
		<spring:message code="auditRecord.formatDate" var="formatDate" />
		<fmt:formatDate value="${row.getTime()}" pattern="${formatDate}" />
	</display:column>

	<spring:message code="auditRecord.list.titleHeader" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />

	<spring:message code="auditRecord.list.descriptionHeader"
		var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}"
		sortable="false" />

	<spring:message code="auditRecord.list.attachmentHeader"
		var="attachmentHeader" />
	<display:column property="attachments" title="${attachmentHeader}"
		sortable="false" />

	<security:authorize access="hasRole('AUDITOR')">
		<spring:message code="auditRecord.list.draftHeader" var="draftHeader" />
		<display:column property="draft" title="${draftHeader}"
			sortable="false" />

		<spring:message code="auditRecord.list.editHeader" var="editHeader" />
		<display:column title="" sortable="false">
			<jstl:if test="${row.isDraft()}">
				<spring:url
					value="auditrecord/auditor/edit.do?auditRecordId=${row.getId()}"
					var="editLink" />
				<a href="${editLink}"> 
					<jstl:out value="${editHeader}" />
				</a>
			</jstl:if>
		</display:column>
	</security:authorize>
</display:table>

<spring:message code="auditRecord.list.back" var="listBack" />
<input type="button" value="${listBack}"
	onClick="javascript:relativeRedir('trip/list.do');" />
	
<jstl:if test="${not empty param.tripId}">
<security:authorize access="hasRole('AUDITOR')">
	<spring:message code="auditRecord.list.create" var="listCreate" />
	<input type="button" value="${listCreate}"
		onClick="javascript: relativeRedir('auditrecord/auditor/create.do?tripId=${param.tripId}');" />
</security:authorize>
	</jstl:if>



