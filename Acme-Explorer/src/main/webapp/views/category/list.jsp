<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<jstl:if test="${category.getName() != \"CATEGORY\"}">	
	<security:authorize access="hasRole('ADMINISTRATOR')">
		<h2>
			<jstl:out value="${category.getName()}" />
			 - 
			<a href="category/administrator/edit.do?categoryId=${category.getId()}"><spring:message code="category.list.editCategory" /></a>	
		</h2>
	</security:authorize>
	
	<security:authorize access="!hasRole('ADMINISTRATOR')">
		<h2><jstl:out value="${category.getName()}" /></h2>
	</security:authorize>
</jstl:if>

<ul>
	<jstl:if test="${parentCategory != null}">
		<li>
			<a href="category/list.do?categoryId=${parentCategory.getId()}">
				<spring:message code="category.list.backToParent" />
				
				<jstl:if test="${parentCategory.getName() != \"CATEGORY\"}">
					<b><jstl:out value="${parentCategory.getName()}" /></b>	
				</jstl:if>
				
				<jstl:if test="${parentCategory.getName() == \"CATEGORY\"}">
					<spring:message code="category.list.root" />
				</jstl:if>
			</a>
		</li>
		<br>
	</jstl:if>

	<jstl:forEach items="${category.getChildrenCategories()}" var="c">
		<li>
			<a href="category/list.do?categoryId=${c.getId()}">
				<b><jstl:out value="${c.getName()}" /></b>
			</a>
			 -
			<a href="trip/list.do?categoryId=${c.getId()}">
				<spring:message code="category.list.searchTrips" />
			</a>
		</li>
	</jstl:forEach>
</ul>

<security:authorize access="hasRole('ADMINISTRATOR')">
	<a href="category/administrator/create.do?parentCategoryId=${category.getId()}"><spring:message code="category.list.createCategory" /></a>
</security:authorize>
