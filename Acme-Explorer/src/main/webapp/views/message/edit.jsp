<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<form:form method="POST" action="message/edit.do" modelAttribute="myMessage">
	
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="sender" />
	<form:hidden path="moment" />
	<form:hidden path="isSpam" />
	<form:hidden path="priority" />
	
	<jstl:if test="${isNotification == true}">
		<jstl:forEach items="${actors}" var="actor" varStatus="status">
			<jstl:if test="${status.index == 0}">
				<jstl:set var="actorsIds" value="${actor.getId()}" />
			</jstl:if>
			<jstl:if test="${status.index != 0}">
				<jstl:set var="actorsIds" value="${actorsIds},${actor.getId()}" />
			</jstl:if>
		</jstl:forEach>
		<input id="recipients" name="recipients" type ="hidden" value="${actorsIds}" />
		<form:errors path="recipients" cssClass="error" />
	</jstl:if>
		
	<jstl:if test="${isNotification == false}">
		<form:label path="recipients">
			<spring:message code="message.edit.recipients"/>
		</form:label>
		<form:select path="recipients" multiple="multiple">
			<jstl:forEach items="${actors}" var="actor">
				<jstl:set var="actorSelected" value="" />
				<jstl:if test="${myMessage.getRecipients().contains(actor)}">
					<jstl:set var="actorSelected" value="selected" />
				</jstl:if>
				<form:option label="${actor.getUserAccount().getUsername()} | ${actor.getName()} ${actor.getSurname()}" 
				value="${actor.id}" selected="${actorSelected}" />
			</jstl:forEach>
		</form:select>
		<form:errors path="recipients" cssClass="error" />
	</jstl:if>
	
	<br />
	
	<form:label path="priority.priority">
		<spring:message code="message.edit.priority"/>
	</form:label>
	<form:select path="priority.priority">
		<form:option value="NEUTRAL"><spring:message code="message.edit.priority.neutral" /></form:option>
		<form:option value="LOW"><spring:message code="message.edit.priority.low" /></form:option>
		<form:option value="HIGH"><spring:message code="message.edit.priority.high" /></form:option>
	</form:select>
	<form:errors path="priority.priority" cssClass="error" />
	
	<br />
	
	<form:label path="subject">
		<spring:message code="message.edit.subject"/>
	</form:label>
	<form:input path="subject" />
	<form:errors path="subject" cssClass="error" />
	
	<br />
	
	<form:label path="body">
		<spring:message code="message.edit.body"/>
	</form:label>
	<form:textarea path="body" />
	<form:errors path="body" cssClass="error" />
	
	<br />
	
	<input type="hidden" name="isNotification" value="${isNotification}" />
	
	<spring:message code="message.edit.send" var="sendButton" />
	<input type="submit" name="send" value="${sendButton}" />
	
	<spring:message code="message.edit.cancel" var="editCancel"/>
	<input type="button" value="${editCancel}" onClick="javascript: relativeRedir('folder/list.do');" />
	
</form:form>