<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p>
	<spring:message code="ranger.edit.name" var="name"/>
	<b><jstl:out value="${name }: "/></b>
	<jstl:out value="${ranger.getName()}" />
</p>

<p>
	<spring:message code="ranger.edit.surname" var="surname" />
	<b><jstl:out value="${surname }: "/></b>
	<jstl:out value="${ranger.getSurname()}" />
</p>

<p>
	<spring:message code="ranger.edit.email" var="email"/>
	<b><jstl:out value="${email }: "/></b>
	<jstl:out value="${ranger.getEmail()}" />
</p>

<p>
	<jstl:if test="${ranger.getPhone() != null && StringUtils.isBlank(ranger.getPhone())}">
	<spring:message code="ranger.edit.phone" var="phone"/>
	<b><jstl:out value="${phone }: "/></b>
	<jstl:out value="${ranger.getPhone()}" />
	</jstl:if>
</p>

<p>
	<jstl:if test="${ranger.getAddress() != null && StringUtils.isBlank(ranger.getAddress())}">
	<spring:message code="ranger.edit.address" var="address"/>
	<b><jstl:out value="${address }: "/></b>
	<jstl:out value="${ranger.getAddress()}" />
	</jstl:if>
</p>
	<jstl:if test="${!ranger.getCurriculum().equals(null)}">
		<a href="curriculum/display.do?curriculumId=${ranger.getCurriculum().getId()}"><spring:message
					code="ranger.display.curriculum" /></a>
	</jstl:if>
	
		<jstl:if test="${!ranger.getSocialIdentities().isEmpty() }" >

	<display:table name="socialIdentities" id="row" requestURI="ranger/display.do"
		pagesize="5">
		<spring:message code="ranger.socialidentity.nick" var="nick" />
		<display:column property="nick" title="${nick}" sortable="true" />
	
		<spring:message code="ranger.socialidentity.netName" var="netName" />
		<display:column property="netName" title="${netName}" sortable="true" />
		
		<spring:message code="ranger.socialidentity.link" var="link" />
					<spring:url value="${row.getLink()}" var="rowLink" />
		<display:column title="${link}" sortable="true" >	
					<a href="${rowLink}"> 
				<jstl:out value="${rowLink}" />
					</a>
		</display:column>
		
		<spring:message code="ranger.socialidentity.image" var="photo" />
		<display:column title="${photo }" sortable="false">
			<spring:url value="${row.getPhoto()}" var="imageLink" />
				<img src="${imageLink}" height="100" width="100" />
		</display:column><br>
	</display:table>
</jstl:if>
	
