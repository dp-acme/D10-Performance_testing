package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Category;
import domain.LegalText;
import domain.Manager;
import domain.Ranger;
import domain.Stage;
import domain.TagValue;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml" })
@Transactional
public class TripServiceTest extends AbstractTest {

	@Autowired
	private TripService tripService;

	@Autowired
	private ManagerService managerService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private LegalTextService legalTextService;

	@Autowired
	private RangerService rangerService;

	@Autowired
	private StageService stageService;

	@Autowired
	private TagValueService tagValueService;

	// ***CRUD TEST*****

	@Test
	public void testFindAll() {
		Collection<Trip> result;

		result = tripService.findAll();

		Assert.notNull(result);

		this.unauthenticate();
	}

	@Test
	public void testFindOne() {
		List<Trip> all;
		Trip tripFromList, tripFromRepository;

		all = (List<Trip>) tripService.findAll();
		Assert.notEmpty(all);
		tripFromList = all.get(0);

		tripFromRepository = tripService.findOne(tripFromList.getId());

		Assert.notNull(tripFromRepository);
		Assert.isTrue(tripFromList.equals(tripFromRepository));

		this.unauthenticate();
	}

	@Test
	public void testSave() {
		Trip trip, saved;
		List<Manager> managers;

		List<Category> categories;
		List<LegalText> legalTexts;
		List<TagValue> tags;
		List<Ranger> rangers;
		Stage stage;
		

		managers = (List<Manager>) managerService.findAll();
		this.authenticate(managers.get(0).getUserAccount().getUsername());

		categories = (List<Category>) categoryService.findAll();
		legalTexts = (List<LegalText>) legalTextService.findAll();
		tags = (List<TagValue>) tagValueService.findAll();
		rangers = (List<Ranger>) rangerService.findAll();


		Assert.notEmpty(managers);
		Assert.notEmpty(categories);
		Assert.notEmpty(legalTexts);
		Assert.notEmpty(tags);
		Assert.notEmpty(rangers);

		trip = tripService.create();
		trip.setDescription("Descripcion");
		trip.setEndDate(new Date(System.currentTimeMillis() + 2000000));
		trip.setStartDate(new Date(System.currentTimeMillis() + 1000000));
		trip.setPublicationDate(new Date(System.currentTimeMillis() + 500000));
		trip.setCategory(categories.get(0));
		trip.setLegalText(legalTexts.get(0));
		trip.setRequirements("requirements");

		stage = stageService.create(trip);
		stage.setDescription("Hola");
		stage.setPrice(200.0);
		stage.setTitle("Stage1");

		trip.getStages().add(stage);
		trip.setRanger(rangers.get(0));
		trip.getTags().add(tags.get(0));
		trip.setTitle("Titulo");

		saved = tripService.save(trip);

		Assert.isTrue(tripService.findAll().contains(saved));

		this.unauthenticate();
	}

	@Test
	public void testDelete() {
		Trip trip, saved;
		List<Manager> managers;

		List<Category> categories;
		List<LegalText> legalTexts;
		List<TagValue> tags;
		List<Ranger> rangers;
		Stage stage;

		managers = (List<Manager>) managerService.findAll();
		this.authenticate(managers.get(0).getUserAccount().getUsername());

		categories = (List<Category>) categoryService.findAll();
		legalTexts = (List<LegalText>) legalTextService.findAll();
		tags = (List<TagValue>) tagValueService.findAll();
		rangers = (List<Ranger>) rangerService.findAll();


		Assert.notEmpty(managers);
		Assert.notEmpty(categories);
		Assert.notEmpty(legalTexts);
		Assert.notEmpty(tags);
		Assert.notEmpty(rangers);

		trip = tripService.create();
		trip.setDescription("Descripcion");
		trip.setEndDate(new Date(System.currentTimeMillis() + 2000000));
		trip.setStartDate(new Date(System.currentTimeMillis() + 1000000));
		trip.setPublicationDate(new Date(System.currentTimeMillis() + 500000));
		trip.setCategory(categories.get(0));
		trip.setLegalText(legalTexts.get(0));
		trip.setRequirements("requirements");

		stage = stageService.create(trip);
		stage.setDescription("Hola");
		stage.setPrice(200.0);
		stage.setTitle("Stage1");

		trip.getStages().add(stage);
		trip.setRanger(rangers.get(0));
		trip.getTags().add(tags.get(0));
		trip.setTitle("Titulo");

		saved = tripService.save(trip);

		Assert.isTrue(tripService.findAll().contains(saved));

		tripService.delete(saved);

		Assert.isTrue(!tripService.findAll().contains(saved));

		this.unauthenticate();
	}

	// **** TEST ESPECÍFICOS ****

	@Test
	public void testCancel() {
		List<Trip> trips;
		Trip trip;

		trips = (List<Trip>) tripService.findAll();

		Assert.notEmpty(trips);

		trip = null;

		for (Trip t : trips) {
			if (t.getStartDate().after(new Date())
					&& t.getPublicationDate().before(new Date())) {
				trip = t;
				break;
			}
		}

		Assert.notNull(trip);
		Assert.isNull(trip.getCancellationReason());

		this.authenticate(trip.getManager().getUserAccount().getUsername());

		tripService.cancel(trip.getId(), "Cancellation reason test");

		Assert.notNull(trip.getCancellationReason());

		this.unauthenticate();
	}

	@Test
	public void testFilterTrips() {
		this.authenticate("Explorer1");

		Collection<Trip> trips;

		trips = tripService.filterTripsByCriteria("W", new Date(), null, null,
				null); // Trips que tengan una fecha de comienzo mayor a la
						// actual

		Assert.notNull(trips);

		this.unauthenticate();
	}
}
