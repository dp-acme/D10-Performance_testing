package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Manager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class ManagerServiceTest  extends AbstractTest{

	@Autowired 
	private ManagerService managerService;

	//***CRUD TEST*****
	
	@Test
	public void testFindAll(){
		Collection<Manager> result;
		
		result = managerService.findAll();
		
		Assert.notNull(result);
		Assert.notEmpty(result);
	}
	
	@Test
	public void testFindOne(){
		List<Manager> all;
		Manager managerFromList, managerFromRepository;
		
		all = (List<Manager>) managerService.findAll();
		Assert.notEmpty(all);
		managerFromList = all.get(0);
		
		managerFromRepository = managerService.findOne(managerFromList.getId());
		
		Assert.notNull(managerFromRepository);
		Assert.isTrue(managerFromList.equals(managerFromRepository));
	}
	
	@Test
	public void testCreate(){
		Manager manager, result;
		UserAccount userAccount;
		List<Authority> authorities;
		Authority auth;
		
		authenticate("admin");

		authorities = new ArrayList<Authority>();
		auth = new Authority();
		auth.setAuthority(Authority.MANAGER);
		authorities.add(auth);
		userAccount = new UserAccount();
		userAccount.setUsername("testUsername");
		userAccount.setPassword("testPassword");
		userAccount.setAuthorities(authorities);
		
		manager = managerService.create();
		manager.setUserAccount(userAccount);
		manager.setAddress("address");
		manager.setEmail("email@acme.es");
		manager.setName("testManager");
		manager.setPhone("616111111");
		manager.setSurname("testSurname");
		result = managerService.save(manager);
		
		Assert.notNull(result);
		Assert.isTrue(managerService.findAll().contains(result));
		Assert.isTrue(managerService.findOne(result.getId()) != null);
		
		authenticate(null);
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testCreateNoAuth(){
		Manager manager, result;
		UserAccount userAccount;
		List<Authority> authorities;
		Authority auth;

		authorities = new ArrayList<Authority>();
		auth = new Authority();
		auth.setAuthority(Authority.MANAGER);
		authorities.add(auth);
		userAccount = new UserAccount();
		userAccount.setUsername("testUsername");
		userAccount.setPassword("testPassword");
		userAccount.setAuthorities(authorities);
		
		manager = managerService.create();
		manager.setUserAccount(userAccount);
		manager.setAddress("address");
		manager.setEmail("email@acme.es");
		manager.setName("testManager");
		manager.setPhone("616111111");
		manager.setSurname("testSurname");
		result = managerService.save(manager);
		
		Assert.notNull(result);
		Assert.isTrue(!managerService.findAll().contains(result));
		Assert.isTrue(managerService.findOne(result.getId()) == null);
	}
	
	@Test
	public void testSave(){
		Manager manager, result;
		String emailOld, emailNew;
		List<Manager> all;

		all = (List<Manager>) managerService.findAll();
		manager = all.get(0);
		
		authenticate(manager.getUserAccount().getUsername());
		
		emailOld = manager.getEmail();
		manager.setEmail("newemailfortesting@acme.com");
		result = managerService.save(manager);
		
		result = managerService.findOne(result.getId());
		emailNew = result.getEmail();
		
		Assert.notNull(result);
		Assert.isTrue(managerService.findAll().contains(result));
		Assert.isTrue(managerService.findOne(result.getId()) != null);
		Assert.isTrue(!emailOld.equals(emailNew) && emailNew.equals("newemailfortesting@acme.com"));
		
		authenticate(null);
	}
	
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testSaveNoAuth(){
		Manager manager, result;
		String emailOld, emailNew;
		List<Manager> all;

		all = (List<Manager>) managerService.findAll();
		manager = all.get(0);
		
		authenticate("admin");
		
		emailOld = manager.getEmail();
		manager.setEmail("newemailfortesting@acme.com");
		result = managerService.save(manager);
		
		result = managerService.findOne(result.getId());
		emailNew = result.getEmail();
		
		Assert.notNull(result);
		Assert.isTrue(managerService.findAll().contains(result));
		Assert.isTrue(managerService.findOne(result.getId()) != null);
		Assert.isTrue(!emailOld.equals(emailNew) && emailNew.equals("newemailfortesting@acme.com"));
		
		authenticate(null);
	}
}
