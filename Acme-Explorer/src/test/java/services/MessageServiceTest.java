
package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import utilities.AbstractTest;
import domain.Actor;
import domain.ApplyFor;
import domain.Explorer;
import domain.Folder;
import domain.FolderType;
import domain.Message;
import domain.Priority;
import domain.Status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class MessageServiceTest extends AbstractTest {

	@Autowired
	private MessageService	messageService;

	@Autowired
	private ActorService	actorService;

	@Autowired
	private FolderService	folderService;

	@Autowired
	private ExplorerService	explorerService;


	@Test
	public void findAllMessagesTest() {
		List<Message> messages;

		messages = (List<Message>) this.messageService.findAll();

		Assert.notNull(messages);
	}

	@Test
	public void findMessageTest() {
		List<Message> messages;
		Message message;

		messages = (List<Message>) this.messageService.findAll();
		Assert.notNull(messages);
		message = messages.get(0);

		Assert.isTrue(this.messageService.findAll().contains(this.messageService.findOne(message.getId())));
	}

	@Test
	public void deleteTest() {
		Message message;
		List<Message> messages;
		Folder origin;
		Actor actor;
		int sizeStart, sizeEnd;

		this.authenticate("manager2");

		actor = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		origin = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.INBOX);

		messages = (List<Message>) origin.getMessages();
		message = messages.get(0);

		sizeStart = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.TRASHBOX).getMessages().size();

		this.messageService.delete(message, actor);

		sizeEnd = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.TRASHBOX).getMessages().size();

		Assert.isTrue(sizeStart + 1 == sizeEnd);

		message = this.messageService.findOne(message.getId());

		sizeStart = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.TRASHBOX).getMessages().size();

		this.messageService.delete(message, actor);

		sizeEnd = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.TRASHBOX).getMessages().size();
		Assert.isTrue(sizeStart - 1 == sizeEnd && !this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.TRASHBOX).getMessages().contains(message));
	}

	@Test
	public void sendMessageTest() {
		this.authenticate("manager1");
		Message result;
		Collection<Actor> recipients;
		String subject;
		String body;
		Priority priority;
		Actor sender;

		priority = new Priority();
		priority.setPriority(Priority.LOW);
		subject = "Subject";
		body = "Body";
		sender = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		recipients = this.actorService.findAll();
		recipients.remove(sender);

		result = this.messageService.create(sender);
		result.setBody(body);
		result.setRecipients(recipients);
		result.setSubject(subject);
		result.setPriority(priority);

		result = this.messageService.sendMessage(result, false);

		Assert.isTrue(this.folderService.getFolderByActorIdAndType(sender.getId(), FolderType.OUTBOX).getMessages().contains(result));

		for (final Actor a : recipients)
			Assert.isTrue(this.folderService.getFolderByActorIdAndType(a.getId(), FolderType.INBOX).getMessages().contains(result));
	}

	@Test
	public void sendMessageNotificationTest() {
		this.authenticate("manager1");
		
		Message result;
		Collection<Actor> recipients;
		String subject;
		String body;
		Priority priority;
		Actor sender;

		priority = new Priority();
		priority.setPriority(Priority.LOW);
		subject = "Subject";
		body = "Body";
		sender = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		recipients = this.actorService.findAll();

		result = this.messageService.create(sender);
		result.setBody(body);
		result.setRecipients(recipients);
		result.setSubject(subject);
		result.setPriority(priority);

		result = this.messageService.sendMessage(result, true);

		Assert.isTrue(this.folderService.getFolderByActorIdAndType(sender.getId(), FolderType.NOTIFICATIONBOX).getMessages().contains(result));

		for (final Actor a : recipients)
			Assert.isTrue(this.folderService.getFolderByActorIdAndType(a.getId(), FolderType.NOTIFICATIONBOX).getMessages().contains(result));
	}

	@Test
	public void sendSpamMessageTest() {
		this.authenticate("manager1");
		Message result;
		Collection<Actor> recipients;
		String subject;
		String body;
		Priority priority;
		Actor sender;

		priority = new Priority();
		priority.setPriority(Priority.LOW);
		subject = "Subject";
		body = "Sex";
		sender = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		recipients = this.actorService.findAll();
		recipients.remove(sender);

		result = this.messageService.create(sender);
		result.setBody(body);
		result.setRecipients(recipients);
		result.setSubject(subject);
		result.setPriority(priority);

		result = this.messageService.sendMessage(result, false);

		Assert.isTrue(this.folderService.getFolderByActorIdAndType(sender.getId(), FolderType.OUTBOX).getMessages().contains(result));

		for (final Actor a : recipients)
			Assert.isTrue(this.folderService.getFolderByActorIdAndType(a.getId(), FolderType.SPAMBOX).getMessages().contains(result));
	}

	@Test
	public void statusChangeTest() {
		ApplyFor applyFor;
		Explorer explorer;
		Status status1;
		Status status2;
		int sizeStartM, sizeEndM, sizeEndE, sizeStartE;

		status1 = new Status();
		status1.setStatus(Status.ACCEPTED);
		status2 = new Status();
		status2.setStatus(Status.CANCELLED);

		explorer = null;
		applyFor = null;
		for(Explorer e: explorerService.findAll()){
			for(ApplyFor ap : e.getApplications()){
				if(ap.getTrip().getStartDate().after(new Date())&&ap.getStatus().equals(Status.ACCEPTED)){
					applyFor = ap;
					explorer = e;
					break;
				}
			}
			if(applyFor!=null){
				break;
			}
		}

		if(explorer!=null&&applyFor!=null){
			this.authenticate(explorer.getUserAccount().getUsername().toString());

		sizeStartM = this.folderService.getFolderByActorIdAndType(applyFor.getTrip().getManager().getId(), FolderType.NOTIFICATIONBOX).getMessages().size();
		sizeStartE = this.folderService.getFolderByActorIdAndType(applyFor.getExplorer().getId(), FolderType.NOTIFICATIONBOX).getMessages().size();
		this.explorerService.cancelApplyFor(applyFor.getTrip().getId());

		sizeEndM = this.folderService.getFolderByActorIdAndType(applyFor.getTrip().getManager().getId(), FolderType.NOTIFICATIONBOX).getMessages().size();
		sizeEndE = this.folderService.getFolderByActorIdAndType(applyFor.getExplorer().getId(), FolderType.NOTIFICATIONBOX).getMessages().size();

		Assert.isTrue(sizeStartM + 1 == sizeEndM && sizeStartE + 1 == sizeEndE);
		}

	}

	@Test
	public void changeFolderTest() {
		Message message;
		List<Message> messages;
		Folder destination, origin;
		Actor actor;
		int sizeStartDest, sizeEndDest, sizeStartOri, sizeEndOri;

		this.authenticate("manager2");

		actor = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		origin = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.INBOX);

		messages = (List<Message>) origin.getMessages();
		message = messages.get(0);

		destination = this.folderService.create(actor);
		destination.setName("Test Folder");
		destination = this.folderService.save(destination);

		Assert.notNull(this.folderService.findOne(destination.getId()));

		sizeStartDest = destination.getMessages().size();
		sizeStartOri = origin.getMessages().size();

		this.messageService.changeFolder(message, destination, actor);

		sizeEndDest = destination.getMessages().size();
		sizeEndOri = origin.getMessages().size();

		origin = this.folderService.findOne(origin.getId());
		destination = this.folderService.findOne(destination.getId());

		Assert.isTrue(sizeStartDest + 1 == sizeEndDest);
		Assert.isTrue(sizeStartOri - 1 == sizeEndOri);
		Assert.isTrue(!origin.getMessages().contains(message) && destination.getMessages().contains(message));
	}

}
