
package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.LegalText;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class LegalTextServiceTest extends AbstractTest {

	//Service under test
	@Autowired
	private LegalTextService		legalTextService;

	// Otros servicios

	@Test
	public void testCreate() {
		LegalText legalText;

		legalText = this.legalTextService.create();
		legalText.setMoment(new Date(System.currentTimeMillis() - 1));

	}
	@SuppressWarnings("deprecation")
	@Test
	public void testSave() {
		LegalText legalText, saved;
		Collection<LegalText> legalTexts;

		legalText = this.legalTextService.create();
		// Seteamos todos los atributos
		legalText.setTitle("Leyes consitucion");
		legalText.setBody("El texto de esta ley..");
		legalText.setMoment(new Date("10/11/2016 16:40"));
		legalText.setLaws("Ley ");
		legalText.setFinalMode(false);

		saved = this.legalTextService.save(legalText); // guardamos en el repositorio el socialIdentity creado
		legalTexts = this.legalTextService.findAll(); // cogemos todos los socialIdentities 

		Assert.isTrue(legalTexts.contains(saved)); // comprobamos que el socialIdentity que hemos creado existe en la BD

	}
	@Test
	public void testFindOne() {
		// Meto todos los legalText en una lista:
		List<LegalText> listLegalText;
		listLegalText = (List<LegalText>) this.legalTextService.findAll();
		this.legalTextService.findOne(listLegalText.get(0).getId());
	}
	@Test(expected = IllegalArgumentException.class)
	public void testDeleteNoDraft() {
		List<LegalText> listLegalText;

		//listAdministrator = new ArrayList<Administrator>();

		this.authenticate("admin");

		listLegalText = (List<LegalText>) this.legalTextService.findAll();

		this.legalTextService.delete(listLegalText.get(0));

	}

	@Test
	public void testDeleteDraft() {
		List<LegalText> listLegalText;

		this.authenticate("admin");

		listLegalText = (List<LegalText>) this.legalTextService.findAll();
		this.legalTextService.delete(listLegalText.get(1));

	}

	@Test
	public void testFindAll() {
		Collection<LegalText> result;

		result = this.legalTextService.findAll();
		Assert.notNull(result);

	}
}
