package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Auditor;
import domain.Note;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml" })
@Transactional
public class NoteServiceTest extends AbstractTest {

	@Autowired
	private NoteService noteService;

	@Autowired
	private ActorService actorService;

	@Autowired
	private TripService tripService;

	// ***CRUD TEST*****

	@Test
	public void testSaveNewNote() {
		Note note, result;
		UserAccount userAccount;
		Auditor auditor;
		List<Trip> trips;
		Collection<Note> notes;

		super.authenticate("auditor1");
		userAccount = LoginService.getPrincipal();
		auditor = (Auditor) actorService.findByUserAccountId(userAccount
				.getId());
		trips = new ArrayList<Trip>(tripService.findAll());
		note = noteService.create(auditor, trips.get(0));
		note.setAuditorRemark("testAuditorRemark1");
		result = noteService.save(note);
		super.authenticate(null);
		super.authenticate(note.getTrip().getManager().getUserAccount()
				.getUsername());
		notes = noteService.findAll();
		Assert.isTrue(notes.contains(result));
		super.authenticate(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSaveNewNoteBadAuth() {
		Note note, result;
		UserAccount userAccount;
		Auditor auditor;
		List<Trip> trips;
		Collection<Note> notes;

		super.authenticate("auditor1");
		userAccount = LoginService.getPrincipal();
		auditor = (Auditor) actorService.findByUserAccountId(userAccount.getId());
		super.authenticate(null);
		
		super.authenticate("manager1");
		trips = new ArrayList<Trip>(tripService.findAll());
		note = noteService.create(auditor, trips.get(0));
		note.setAuditorRemark("testAuditorRemark1");
		result = noteService.save(note);
		super.authenticate(null);
		super.authenticate(note.getTrip().getManager().getUserAccount()
				.getUsername());
		notes = noteService.findAll();
		Assert.isTrue(notes.contains(result));
		super.authenticate(null);
	}

	@Test
	public void testFindOne() {
		UserAccount userAccount;
		Auditor auditor;
		Collection<Note> notes;
		Note note, savedNote, result;
		List<Trip> trips;

		super.authenticate("auditor1");
		userAccount = LoginService.getPrincipal();
		auditor = (Auditor) actorService.findByUserAccountId(userAccount
				.getId());
		trips = new ArrayList<Trip>(tripService.findAll());
		note = noteService.create(auditor, trips.get(0));
		note.setAuditorRemark("testAuditorRemark1");
		savedNote = noteService.save(note);
		result = noteService.findOne(savedNote.getId());
		notes = auditor.getNotes();
		Assert.isTrue(notes.contains(result));

		super.authenticate(null);
	}
	
	@Test(expected = IllegalArgumentException.class)	
	public void testFindOneBadAuth() {
		UserAccount userAccount;
		Auditor auditor;
		Collection<Note> notes;
		Note note, savedNote, result;
		List<Trip> trips;

		super.authenticate("auditor1");
		userAccount = LoginService.getPrincipal();
		auditor = (Auditor) actorService.findByUserAccountId(userAccount
				.getId());
		super.authenticate(null);

		super.authenticate("manager1");

		trips = new ArrayList<Trip>(tripService.findAll());
		note = noteService.create(auditor, trips.get(0));
		note.setAuditorRemark("testAuditorRemark1");
		savedNote = noteService.save(note);
		result = noteService.findOne(savedNote.getId());
		notes = auditor.getNotes();
		Assert.isTrue(notes.contains(result));

		super.authenticate(null);
	}


	@Test
	public void testFindAll() {
		Collection<Note> notes;

		super.authenticate("manager1");
		notes = noteService.findAll();
		Assert.isTrue(notes.size() == 2);
		super.authenticate(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFindAllBadAuth() {
		Collection<Note> notes;

		super.authenticate("auditor1");
		notes = noteService.findAll();
		Assert.isTrue(notes.size() == 2);
		super.authenticate(null);
	}

	@Test
	public void testReplyExistingNote() {
		Note note, result;
		List<Note> notes;

		super.authenticate("manager1");
		notes = (List<Note>) noteService.findAll();
		note = null;
		for (Note n : notes) {
			if (n.getManagerReply() == null) {
				note = n;
				break;
			}
		}
		note.setManagerReply("testManagerReply1");
		result = noteService.save(note);
		Assert.isTrue(notes.contains(result));
		super.authenticate(null);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testReplyExistingNoteBadAuth() {
		Note note, result;
		List<Note> notes;

		super.authenticate("auditor1");
		notes = (List<Note>) noteService.findAll();
		note = null;
		for (Note n : notes) {
			if (n.getManagerReply() == null) {
				note = n;
				break;
			}
		}
		note.setManagerReply("testManagerReply1");
		result = noteService.save(note);
		Assert.isTrue(notes.contains(result));
		super.authenticate(null);

	}

}
