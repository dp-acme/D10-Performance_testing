package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Category;
import domain.LegalText;
import domain.Manager;
import domain.Ranger;
import domain.Stage;
import domain.TagValue;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:spring/datasource.xml",
		"classpath:spring/config/packages.xml"})
@Transactional



public class StageServiceTest extends AbstractTest{
	
	@Autowired 
	private TripService tripService;
	
	@Autowired 
	private ManagerService managerService;
	
	@Autowired 
	private CategoryService categoryService;
	
	@Autowired 
	private LegalTextService legalTextService;

	@Autowired 
	private RangerService rangerService;

	@Autowired 
	private StageService stageService;
	
	@Autowired 
	private TagValueService tagValueService;
	
	//***CRUD TEST*****
	
	@Test
	public void testFindAll(){
		Collection<Stage> result;
		
		result = stageService.findAll();
		
		Assert.notNull(result);
		
		this.unauthenticate();
	}
	
	@Test
	public void testFindOne(){
		List<Stage> all;
		Stage stageFromList, stageFromRepository;
		
		all = (List<Stage>) stageService.findAll();
		Assert.notEmpty(all);
		stageFromList = all.get(0);
		
		stageFromRepository = stageService.findOne(stageFromList.getId());
		
		Assert.notNull(stageFromRepository);
		Assert.isTrue(stageFromList.equals(stageFromRepository));
		
		this.unauthenticate();
	}
	
	@Test
	public void testSave(){
		Trip trip, saved;
		List<Manager> managers;
		
		List<Category> categories;
		List<LegalText> legalTexts;
		List<TagValue> tags;
		List<Ranger> rangers;
		Stage stage;
		
		managers = (List<Manager>) managerService.findAll();
		this.authenticate(managers.get(0).getUserAccount().getUsername());

		categories = (List<Category>) categoryService.findAll();
		legalTexts = (List<LegalText>) legalTextService.findAll();
		tags = (List<TagValue>) tagValueService.findAll();
		rangers = (List<Ranger>) rangerService.findAll();
		
		
		Assert.notEmpty(managers);
		Assert.notEmpty(categories);
		Assert.notEmpty(legalTexts);
		Assert.notEmpty(tags);
		Assert.notEmpty(rangers);
		
		trip = tripService.create();
		trip.setDescription("Descripcion");
		trip.setEndDate(new Date(System.currentTimeMillis() + 2000000));
		trip.setStartDate(new Date(System.currentTimeMillis() + 1000000));
		trip.setPublicationDate(new Date(System.currentTimeMillis() + 500000));
		trip.setCategory(categories.get(0));
		trip.setLegalText(legalTexts.get(0));
		trip.setRequirements("requirements");
		
		stage = stageService.create(trip);
		stage.setDescription("Hola");
		stage.setPrice(200.0);
		stage.setTitle("Stage1");
		
		trip.getStages().add(stage);
		trip.setRanger(rangers.get(0));
		trip.getTags().add(tags.get(0));
		trip.setTitle("Titulo");
		
		saved = tripService.save(trip);
						
		Assert.isTrue(tripService.findOne(saved.getId()).getStages().size() == 1);
		
		this.unauthenticate();
	}
	
	@Test
	public void testDelete(){
		Trip trip, saved;
		List<Manager> managers;
		
		List<Category> categories;
		List<LegalText> legalTexts;
		List<TagValue> tags;
		List<Ranger> rangers;
		Stage stage;
		
		managers = (List<Manager>) managerService.findAll();
		this.authenticate(managers.get(0).getUserAccount().getUsername());

		
		categories = (List<Category>) categoryService.findAll();
		legalTexts = (List<LegalText>) legalTextService.findAll();
		tags = (List<TagValue>) tagValueService.findAll();
		rangers = (List<Ranger>) rangerService.findAll();
		
		
		Assert.notEmpty(managers);
		Assert.notEmpty(categories);
		Assert.notEmpty(legalTexts);
		Assert.notEmpty(tags);
		Assert.notEmpty(rangers);
		
		trip = tripService.create();
		trip.setDescription("Descripcion");
		trip.setEndDate(new Date(System.currentTimeMillis() + 2000000));
		trip.setStartDate(new Date(System.currentTimeMillis() + 1000000));
		trip.setPublicationDate(new Date(System.currentTimeMillis() + 500000));
		trip.setCategory(categories.get(0));
		trip.setLegalText(legalTexts.get(0));
		trip.setRequirements("requirements");
		
		stage = stageService.create(trip);
		stage.setDescription("Hola");
		stage.setPrice(200.0);
		stage.setTitle("Stage1");
		
		trip.getStages().add(stage);
		trip.setRanger(rangers.get(0));
		trip.getTags().add(tags.get(0));
		trip.setTitle("Titulo");
		
		saved = tripService.save(trip);
						
		Assert.isTrue(tripService.findOne(saved.getId()).getStages().size() == 1);
		
		stage = ((List<Stage>) tripService.findOne(saved.getId()).getStages()).get(0);
		
		tripService.delete(saved);
		
		Assert.isTrue(!stageService.findAll().contains(stage));
		
		this.unauthenticate();	
	}
}
