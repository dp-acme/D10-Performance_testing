<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<jstl:set var="uri" value="/announcement/list.do" />

<security:authorize access="hasRole('USER')">
	<security:authentication property="principal" var="principal" />
	<jstl:if test="${rendezvous != null && rendezvous.getCreator().getUserAccount().equals(principal) &&
		!rendezvous.isDraft()}">
		<acme:cancel url="/announcement/create.do?rendezvousId=${rendezvous.getId()}" code="announcement.list.create"/>
		<br />
	</jstl:if>
</security:authorize>


<display:table name="announcements" id="row" requestURI="${uri}" pagesize="5">
	<display:column>
		<jstl:out value="${row.getTitle()}" />
		
		<br />
		
		<spring:message code="announcement.formatDate" var="formatDate" />
		<fmt:formatDate value="${row.getMoment()}" pattern="${formatDate}" />
		
		<br />
		
		<a href="rendezvous/display.do?rendezvousId=${row.getRendezvous().getId()}">
			<jstl:out value="${row.getRendezvous().getName()}" />
		</a>
		
		<br />
		<br />
		
		<jstl:out value="${row.getDescription()}" />
		
		<security:authorize access="hasRole('ADMIN')">
			
			<script>
				function confirmDelete(confirmMessage, url) {
					if (confirm(confirmMessage)) {
						relativeRedir(url);
					}
				}
			</script>
			
			<spring:message code="announcement.list.ConfirmDelete" var="confirmMessage" />
			
			<br />
			<button type="button" onclick="javascript: confirmDelete('${confirmMessage}', 'announcement/delete.do?announcementId=${row.getId()}')">
				<spring:message code="announcement.list.delete" />
			</button>
		</security:authorize>
	</display:column>
</display:table>

<jstl:if test="${message != null}">
	<jstl:out value="${message}" />
</jstl:if>
