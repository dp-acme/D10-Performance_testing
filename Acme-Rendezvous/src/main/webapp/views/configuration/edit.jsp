<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form method="POST" action="configuration/edit.do" modelAttribute="configuration">

	<form:hidden path="id" />
	<form:hidden path="version" />
	
	<form:label path="banner">
		<spring:message code="configuration.display.bannerLabel" />:
	</form:label>
	<form:input path="banner" style="width: 500px"/><br/>
	<form:errors cssClass="error" path="banner" /><br/>
	
	
	<form:label path="welcomeMessageEnglish">
		<spring:message code="configuration.display.welcomeMessageEnglishLabel" />:
	</form:label>
	<form:textarea path="welcomeMessageEnglish" style="width: 500px; height: 100px"/>
	
	<form:errors cssClass="error" path="welcomeMessageEnglish" /><br/>
	
	
	<form:label path="welcomeMessageSpanish">
		<spring:message code="configuration.display.welcomeMessageSpanishLabel" />:
	</form:label>
	<form:textarea path="welcomeMessageSpanish" style="width: 500px; height: 100px"/>
	
	<form:errors cssClass="error" path="welcomeMessageSpanish" /><br/>
	
	<form:label path="businessName">
		<spring:message code="configuration.display.businessName" />:
	</form:label>
	<form:input path="businessName" style="width: 300px"/>
	
	<form:errors cssClass="error" path="businessName" /><br/><br/>
	
	
	<input type="submit" name="save" value="<spring:message code="configuration.edit.save" />" />
	<input type="button" value="<spring:message code="configuration.edit.cancel" />" 
		onClick="javascript: relativeRedir('configuration/display.do');" />

</form:form>