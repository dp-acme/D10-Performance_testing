package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CommentRepository;
import security.Authority;
import domain.Actor;
import domain.Comment;
import domain.Rendezvous;
import domain.User;

@Service
@Transactional
public class CommentService {
	// Managed repository ---------------------------------------------------

	@Autowired
	private CommentRepository commentRepository;
	
	// Validator ---------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RendezvousService rendezvousService;
	
	// Constructor ---------------------------------------------------
	
	public CommentService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	public Comment create(User user, Comment parent, Rendezvous rendezvous) {
		Comment result;
		
		result = new Comment();
		result.setUser(user);
		result.setParent(parent);
		result.setRendezvous(rendezvous);
		result.setDeleted(false);
		result.setMoment(new Date(System.currentTimeMillis() - 1));
		result.setReplies(new ArrayList<Comment>());
		
		return result;
	}
	
	public Comment findOne(int commentId) {
		Assert.isTrue(commentId != 0);
		
		Comment result;
		
		result = commentRepository.findOne(commentId);
		
		return result;
	}
	
	public Collection<Comment> findAll() {		
		Collection<Comment> result;
		
		result = commentRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Comment save(Comment comment) {
		Comment result;
		
		Actor principal;
		Authority auth;
				
		principal = actorService.findPrincipal();
		
		auth = new Authority();
		auth.setAuthority(Authority.USER);
		
		Assert.isTrue(principal.getUserAccount().getAuthorities().contains(auth)); //Comprobar que verdaderamente es un User	
		Assert.isTrue(principal instanceof User); //Comprobar que el principal es un User (seguridad de c�digo)
		Assert.isTrue(comment.getUser().equals((User)principal)); //Comprobar que el comentario es suyo
		Assert.isTrue(comment.getRendezvous().getUsers().contains(comment.getUser()) || comment.getRendezvous().getCreator().equals(comment.getUser())); //Comprobar que el usuario est� unido al rendezvous que comenta
		Assert.isTrue(!comment.getRendezvous().isDeleted()); //Comprobar que el rendezvous est� publicado y no borrado
		Assert.isTrue(!comment.getRendezvous().isDraft());
		
		if(comment.getParent() != null){
			Assert.isTrue(comment.getParent().getRendezvous().equals(comment.getRendezvous())); //Comprobar que el Rend. del parent es el mismo
			
			addReply(comment);
		}
		
		result = commentRepository.save(comment);

		return result;
	}
	
	// Other business methods -------------------------------------------------
	
	public Comment flagAsDeleted(Comment comment){
		Assert.isTrue(!comment.getDeleted()); //Comprobar que no se trate de un comentario ya eliminado
		
		Comment result;
		
		Actor principal;
		Authority auth;
		
		principal = actorService.findPrincipal();
		
		auth = new Authority();
		auth.setAuthority(Authority.ADMIN);

		Assert.isTrue(principal.getUserAccount().getAuthorities().contains(auth));
		
		comment.setDeleted(true);
		
		result = commentRepository.save(comment);
		
		return result;
	}

	public Collection<Comment> getRendezvousComments(Integer rendezvousId){
		Collection<Comment> result;
		
		result = commentRepository.getRendezvousComments(rendezvousId);
		Assert.notNull(result);
		
		return result;
	}

	public Collection<Comment> getParentComments(Integer rendezvousId){
		Collection<Comment> result;
		
		result = commentRepository.getParentComments(rendezvousId);
		Assert.notNull(result);
		
		return result;
	}
		
	public Comment reconstruct(Comment prunnedComment, Integer rendezvousId, Integer parentId, BindingResult binding){
		Comment result;
		Comment parent;
		Rendezvous rendezvous;
		Actor principal;

		rendezvous = rendezvousService.findOne(rendezvousId);
		principal = actorService.findPrincipal();
		
		Assert.notNull(rendezvous);
		Assert.notNull(principal);
		
		Assert.isTrue(principal instanceof User);

		if(prunnedComment.getId() == 0){ //Creaci�n
			result = prunnedComment;
			
			result.setReplies(new ArrayList<Comment>());
			result.setRendezvous(rendezvous);
			result.setUser((User) principal);
			
			if(parentId != null){			
				parent = this.findOne(parentId);
			
				Assert.notNull(parent);

				result.setParent(parent);
		
			} else{
				result.setParent(null);			
			}
		
		} else{ //Edici�n
			result = prunnedComment;			
		}
		
		result.setText(prunnedComment.getText());		
		result.setPicture(prunnedComment.getPicture());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	public void addReply(Comment comment){ //A�adir comentario a la lista de respuestas
		Comment parent;
		
		parent = comment.getParent();
		
		Assert.notNull(parent);
		
		parent.getReplies().add(comment);
		
		commentRepository.save(parent);
	}

	public void flush() {
		commentRepository.flush();
	}
}
