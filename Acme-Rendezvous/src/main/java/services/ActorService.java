package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.LoginService;
import domain.Actor;
import domain.User;

@Service
@Transactional
public class ActorService {

	// Managed repository ---------------------------------------------------
	
	@Autowired
	private ActorRepository actorRepository;
	
	// Supporting services ---------------------------------------------------
	
	// Constructor ---------------------------------------------------
	
	public ActorService() {
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Actor findOne(int actorId) {
		Assert.isTrue(actorId != 0);
		
		Actor result;
		
		result = actorRepository.findOne(actorId);
		
		return result;
	}
	
	public Collection<Actor> findAll() {		
		Collection<Actor> result;
		
		result = actorRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public Actor save(Actor actor) {
		Assert.notNull(actor);
		
		Actor result;
		
		result = actorRepository.save(actor);
		
		return result;
	}
	
	// Other business methods ---------------------------------------------------
	
	public Actor findByUserAccountId(int userAccountId) {
		Assert.isTrue(userAccountId != 0);
		
		Actor result;
		
		result = actorRepository.findByUserAccountId(userAccountId);
		
		return result;
	}

	public Actor findPrincipal() {
		return findByUserAccountId(LoginService.getPrincipal().getId());
	}
	
	public User checkIsUser() {
		// Comprueba que el usuario logeado es un user
		Actor actor;
		actor = findPrincipal();

		Assert.isInstanceOf(User.class, actor);
		
		return (User) actor;
	}
	
	public boolean isAdult(Actor actor) {
		Calendar actorAdult = Calendar.getInstance();

		actorAdult.setTime(actor.getBirthday());

		actorAdult.set(actorAdult.get(Calendar.YEAR) + 18,
				actorAdult.get(Calendar.MONTH), actorAdult.get(Calendar.DATE));

		return actorAdult.getTime().before(new Date());
	}
}
