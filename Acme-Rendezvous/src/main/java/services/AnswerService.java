package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AnswerRepository;
import security.LoginService;
import security.UserAccount;
import domain.Answer;
import domain.Question;
import domain.Rendezvous;
import domain.User;

@Service
@Transactional
public class AnswerService {
	
	@Autowired
	private AnswerRepository answerRepository;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private RendezvousService rendezvousService;
	
	@Autowired
	private UserService userService;
	
	public AnswerService(){
		super();
	}
	
	public Answer create(int questionId){
		Answer result;
		User user;
		
		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		result = new Answer();
		
		result.setUser(user);
		result.setQuestion(questionService.findOne(questionId));
		
		return result;
	}
	
	public Answer findOne(int answerId){
		Assert.isTrue(answerId != 0);
		
		Answer result;
		
		result = answerRepository.findOne(answerId);
		
		return result;
	}
	
	public Collection<Answer> findAll(){
		Collection<Answer> result;
		
		result = answerRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public void delete(Answer answer){
		Assert.notNull(answer);	
		
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(answer.getUser().equals(principal));
			
		questionService.deleteAsnwer(answer);
		
		answerRepository.delete(answer);
	}
	
	public Answer save(Answer answer){
		Assert.notNull(answer);
		
		Answer result;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(answer.getUser().getUserAccount().equals(principal));
		
		result = answerRepository.save(answer);
		
		if(answer.getId() == 0){
			questionService.addAnswer(result);
		}

		return result;
	}
	
	public void RVPS(Rendezvous rendezvous, List<String> answers){
		User user = (User) actorService.findPrincipal();
		List<Question> questions = (List<Question>) rendezvous.getQuestions();
		Calendar userAdult = Calendar.getInstance();

		Assert.notNull(rendezvous);
		Assert.isTrue(!rendezvous.isDeleted());
		Assert.isTrue(!rendezvous.isDraft());
		Assert.isTrue(rendezvous.getMoment().after(new Date()));
		Assert.isTrue(!rendezvous.getCreator().equals(user));
		
		Assert.isTrue(questions.size() == answers.size());
		Assert.isTrue(!user.getMyRSVPs().contains(rendezvous));
		
		userAdult.setTime(user.getBirthday());
		
		if(rendezvous.isAdultsOnly())
			userAdult.set(userAdult.get(Calendar.YEAR) + 18,userAdult.get(Calendar.MONTH), userAdult.get(Calendar.DATE));

		Assert.isTrue(userAdult.getTime().before(new Date()));
		
		for(int i = 0; i<questions.size(); i++){
			Question question = questions.get(i);
			Answer answerC = create(question.getId());
			answerC.setAnswer(answers.get(i));
			answerC = save(answerC);
		}
		
		rendezvousService.addUser(rendezvous, user);
	
	}
	
	public void unRVPS(Rendezvous rendezvous){
		User user;
		Collection<Answer> answers;
		
		user = (User) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		
		Assert.isTrue(user.getMyRSVPs().contains(rendezvous));
		
		answers = answerRepository.getAnswerOfRendezvousByUser(rendezvous, user);
				
		rendezvousService.removeUser(rendezvous, user);
		
		user.getMyRSVPs().remove(rendezvous);
		
		userService.save(user);
		
		deleteAll(answers);
	}
	
	public void deleteAll(Collection<Answer> answers){		
		for(Answer a : answers){
			questionService.deleteAsnwer(a);
		}
		answerRepository.deleteInBatch(answers);
	}

}
