package services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.RendezvousRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Administrator;
import domain.Announcement;
import domain.Question;
import domain.Rendezvous;
import domain.Service;
import domain.User;

@org.springframework.stereotype.Service
@Transactional
public class RendezvousService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private RendezvousRepository rendezvousRepository;

	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService actorService;

	@Autowired
	private UserService userService;

	// Constructor------------------------------------------------------------------------

	public RendezvousService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Rendezvous create() {

		// Comprobamos que el usuario logeado es un user
		actorService.checkIsUser();
		// Si es asi lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();

		User user;
		user = (User) this.actorService.findByUserAccountId(principal.getId());

		// Creamos el resultado
		Rendezvous result;
		result = new Rendezvous();

		// Creamos los atributos y relaciones
		// Date moment;
		User creator;
		boolean draft;
		boolean deleted;
		boolean adultsOnly;

		Collection<Question> questions;
		Collection<Announcement> announcements;
		Collection<User> users;
		Collection<Rendezvous> links;

		// Inicializamos los atributos
		// moment = new Date();
		creator = user;
		draft = true;// Inicializamos a true para poder editarlo
		deleted = false;
		adultsOnly = false;

		questions = new ArrayList<Question>();
		announcements = new ArrayList<Announcement>();
		users = new ArrayList<User>();
		// A la lista de usuarios que van a asistir, introducimos el usuario
		links = new ArrayList<Rendezvous>();

		// Asignamos atributos
		// result.setMoment(moment);
		result.setCreator(creator);
		result.setDraft(draft);
		result.setDeleted(deleted);
		result.setAdultsOnly(adultsOnly);

		result.setQuestions(questions);
		result.setAnnouncements(announcements);
		result.setUsers(users);
		result.setLinks(links);

		// Devolvemos resultado
		return result;
	}

	public Rendezvous findOne(final int rendezvousId) {
		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.isTrue(rendezvousId != 0);
		// Creamos el objeto a devolver
		Rendezvous result;
		// Cogemos del repositorio el rendezvous que le pasamos como parametro
		result = this.rendezvousRepository.findOne(rendezvousId);
		// Devolvemos el resultado
		return result;

	}

	public Collection<Rendezvous> findAll() {
		// Creamos el objeto a devolver
		Collection<Rendezvous> result;
		// Cogemos del repositorio los rendezvous
		result = this.rendezvousRepository.findAll();
		// Devolvemos el resultado
		return result;
	}

	public Rendezvous save(Rendezvous rendezvous) {
		// Comprobamos que el rendezvous que le pasamos no es nulo
		Assert.notNull(rendezvous);
		
		Calendar userAdult = Calendar.getInstance();

		// Comprobamos que el usuario logeado es un user
		actorService.checkIsUser();

		// Comprobamos si esta en modo borrador(Draft)
		Assert.isTrue(rendezvous.isDraft());
		
		//No linkear rendezvous borrados o no publicados
		for(Rendezvous r : getRendezvousDraftOrDeleted())
			Assert.isTrue(!rendezvous.getLinks().contains(r));
		
		// Comprobamos si el atributo deleted no esta marcado como borrado
		Assert.isTrue(!rendezvous.isDeleted());

		// Comprobamos que la fecha de moment sea futura
		Assert.isTrue(rendezvous.getMoment().after(new Date()));

		// Si es un user lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();
		User user;

		user = (User) this.actorService.findByUserAccountId(principal.getId());
		userAdult.setTime(user.getBirthday());

		if (rendezvous.isAdultsOnly()) {
			userAdult
					.set(userAdult.get(Calendar.YEAR) + 18,
							userAdult.get(Calendar.MONTH),
							userAdult.get(Calendar.DATE));

			Assert.isTrue(userAdult.getTime().before(new Date()));
		}

		// Comprobamos que el user del rendezvous es el mismo que el que esta
		// logeado
		Assert.isTrue(rendezvous.getCreator().equals(user));

		// Creamos el objeto a devolver
		Rendezvous result;

		// Actualizamos el rendezvous
		result = this.rendezvousRepository.save(rendezvous);

		// Si es la primera vez que entra, a�adimos a la lista de rendezvous el
		// rendezvous que queremos guardar ya que no puede ser nulo y tiene que
		// tenerlo
		if (rendezvous.getId() == 0) {
			user.getMyRendezvouses().add(result);
			this.userService.save(user);
		}

		// Devolvemos el resultado
		return result;
	}

	// Importante acutalizar despues el set deleted por qu tiene que cambiar
	// Tambien es importante si eres admin que si lo eres puedes eliminar un
	// rendezvous

	public void delete(Rendezvous rendezvous) {

		// Comprobamos que el objeto que le pasamos no es nulo
		Assert.notNull(rendezvous);

		// Comprobamos si esta en modo draft

		// Si esta marcado como borrado (Deleted)
		Assert.isTrue(!rendezvous.isDeleted());

		// Si es un user lo cogemos
		UserAccount principal;
		principal = LoginService.getPrincipal();
		Actor actor = this.actorService.findByUserAccountId(principal.getId());

		if (actor instanceof User) {
			actorService.checkIsUser();
			User user = (User) actor;
			Assert.isTrue(rendezvous.isDraft());
			Assert.isTrue(rendezvous.getCreator().equals(user));
		} else
			this.checkIsAdmin();


		rendezvous.setUsers(new ArrayList<User>());

		// Recorrremos los asistentes al rendezvous y para cada uno eliminamos
		// el RSVP
		for (User u : rendezvous.getUsers()) {
			u.getMyRSVPs().remove(rendezvous);
			userService.save(u);

		}

		// Actualizamos el atributo deleted a true
		rendezvous.setDeleted(true);

		rendezvousRepository.save(rendezvous);

	}

	public void addUser(Rendezvous rendezvous, User user) {
		Assert.notNull(rendezvous);
		Assert.notNull(user);

		Assert.isTrue(!user.getMyRSVPs().contains(rendezvous));
		rendezvous.getUsers().add(user);

		user.getMyRSVPs().add(rendezvous);
		user = userService.save(user);
		rendezvous.getUsers().add(user);

		rendezvousRepository.save(rendezvous);
	}

	// Other business methods
	// ---------------------------------------------------

	@Autowired
	private Validator validator;

	public Rendezvous reconstruct(Rendezvous rendezvous, BindingResult binding) {
		Rendezvous _rendezvous;

		if (rendezvous.getId() == 0) {
			UserAccount principal;
			principal = LoginService.getPrincipal();

			User user;
			user = (User) this.actorService.findByUserAccountId(principal.getId());
			_rendezvous = create();
			
			rendezvous.setCreator(user);
			rendezvous.setAnnouncements(_rendezvous.getAnnouncements());
			rendezvous.setDeleted(_rendezvous.isDeleted());
			rendezvous.setDraft(_rendezvous.isDraft());
			rendezvous.setLinks(_rendezvous.getLinks());
			rendezvous.setQuestions(_rendezvous.getQuestions());
			rendezvous.setUsers(_rendezvous.getUsers());
			
		} else {
			_rendezvous = findOne(rendezvous.getId());
			
			rendezvous.setVersion(_rendezvous.getVersion());
			rendezvous.setCreator(_rendezvous.getCreator());
			rendezvous.setAnnouncements(_rendezvous.getAnnouncements());
			rendezvous.setDeleted(_rendezvous.isDeleted());
			rendezvous.setDraft(_rendezvous.isDraft());
			rendezvous.setLinks(_rendezvous.getLinks());
			rendezvous.setQuestions(_rendezvous.getQuestions());
			rendezvous.setUsers(_rendezvous.getUsers());
		}

		validator.validate(rendezvous, binding);

		return rendezvous;
	}

	private void checkIsAdmin() {
		// Comprueba que el usuario logeado es un user
		UserAccount principal;
		principal = LoginService.getPrincipal();

		Actor actor;
		actor = this.actorService.findByUserAccountId(principal.getId());

		Assert.isInstanceOf(Administrator.class, actor);
	}

	// Realizar un metodo que sea publicar que lo que hace es comprobar que el
	// usuario logeado y cambia el set draf que esta marcado como true y pasa a
	// false para publicarlo
	public void publish(Rendezvous rendezvous) {
		// Comprobamos que el usuario logeado es un user
		actorService.checkIsUser();

		// Cogemos el user logeado
		UserAccount principal;
		principal = LoginService.getPrincipal();
		Actor actor = this.actorService.findByUserAccountId(principal.getId());
		User user = (User) actor;

		// Comprobamos que el user del rendezvous es el mismo que esta logeado
		Assert.isTrue(rendezvous.getCreator().equals(user));

		// Comprobamos si no esta publicado
		Assert.isTrue(rendezvous.isDraft());

		// Cambiamos el atributo draft a false para decir que esta publicado
		rendezvous.setDraft(false);
		this.rendezvousRepository.save(rendezvous);
	}

	public void removeUser(Rendezvous rendezvous, User user) {
		Assert.notNull(user);
		Assert.notNull(rendezvous);
		Assert.isTrue(actorService.findByUserAccountId(
				LoginService.getPrincipal().getId()).equals(user));
		Assert.isTrue(rendezvous.getUsers().contains(user));

		rendezvous.getUsers().remove(user);
		user.getMyRSVPs().remove(rendezvous);

		rendezvousRepository.save(rendezvous);
	}

	public Rendezvous reconstructSimilars(Rendezvous rendezvous,
			BindingResult binding) {
		Rendezvous result;

		result = findOne(rendezvous.getId());
		Assert.notNull(result);

		result.setLinks(rendezvous.getLinks() == null ? new ArrayList<Rendezvous>()
				: rendezvous.getLinks());

		validator.validate(result, binding);

		return result;
	}

	// A�adir links de rendezvous similares
	public Rendezvous linkSimilars(Rendezvous rendezvous) {
		Assert.notNull(rendezvous);
		Assert.isTrue(rendezvous.getId() != 0 && !rendezvous.isDeleted());

		Rendezvous result;

		for (Rendezvous similar : rendezvous.getLinks()) {
			Assert.isTrue(!similar.isDraft() && similar.getId() != 0
					&& !similar.isDeleted() && rendezvous != similar);
		}

		result = rendezvousRepository.save(rendezvous);

		return result;
	}

	public Collection<Rendezvous> getRendezvousNoDraft() {
		Collection<Rendezvous> result;

		result = rendezvousRepository.getRendezvousNoDraft();
		Assert.notNull(result);

		return result;
	}

	public Collection<Rendezvous> getRendezvousNoDraftNoDeleted() {
		Collection<Rendezvous> result;

		result = rendezvousRepository.getRendezvousNoDraftNoDeleted();
		Assert.notNull(result);

		return result;
	}

	public Collection<Rendezvous> getMyRendezvousNoDraftNoDeleted(User user) {
		Assert.notNull(user);

		Collection<Rendezvous> result;

		result = rendezvousRepository.getMyRendezvousNoDraftNoDeleted(user
				.getId());
		Assert.notNull(result);

		return result;
	}

	public Collection<Rendezvous> getRendezvousNoDraftNoAdultsOnly() {
		Collection<Rendezvous> result;

		result = rendezvousRepository.getRendezvousNoDraftNoAdultsOnly();
		Assert.notNull(result);

		return result;
	}

	public Rendezvous refreshAnnouncements(Rendezvous rendezvous) {
		Assert.notNull(rendezvous);

		Rendezvous result;

		result = rendezvousRepository.save(rendezvous);

		return result;
	}

	public Collection<Rendezvous> getRSVPSAndMyRendezvous(User user) {
		Assert.notNull(user);

		Collection<Rendezvous> result;

		result = rendezvousRepository.getRSVPSAndMyRendezvous(user.getId());

		return result;
	}

	public Collection<Rendezvous> getRSVPSWithoutDraft(User user) {
		Assert.notNull(user);

		Collection<Rendezvous> result;

		result = rendezvousRepository.getRSVPSWithoutDraft(user.getId());

		return result;
	}

	public Collection<Rendezvous> getRSVPSWithoutDeleted(User user) {
		Assert.notNull(user);

		Collection<Rendezvous> result;

		result = rendezvousRepository.getRSVPSWithoutDraftOrDeleted(user
				.getId());

		return result;
	}

	public Collection<Rendezvous> getRSVPSWithoutDeletedOrAdultsOnly(User user) {
		Assert.notNull(user);

		Collection<Rendezvous> result;

		result = rendezvousRepository
				.getRSVPSWithoutDraftOrDeletedOrAdultsOnly(user.getId());

		return result;
	}

	public Collection<Rendezvous> getMyRendezvousNoDraftNoDeletedNoService(User user, Service service) {
		Assert.notNull(user);
		Assert.notNull(service);

		Collection<Rendezvous> result;

		result = rendezvousRepository.getMyRendezvousNoDraftNoDeletedNoServiceNoPast(user.getId(), service.getId());

		return result;
	}

	public Collection<Rendezvous> getRendezvousNoDraftNoDeletedByCategory(int categoryId) {
		Assert.isTrue(categoryId != 0);

		Collection<Rendezvous> result;

		result = rendezvousRepository.getRendezvousNoDraftNoDeletedByCategory(categoryId);

		return result;
	}
	

	public Collection<Rendezvous> getRendezvousDraftOrDeleted() {
		Collection<Rendezvous> result;

		result = rendezvousRepository.getRendezvousDraftOrDeleted();

		return result;
	}
	
	public void flush() {
		rendezvousRepository.flush();
	}
	
	public Collection<Rendezvous> getRendezvousNoDraftNoDeletedNoAdultsOnlyByCategory(int categoryId) {
		Assert.isTrue(categoryId != 0);

		Collection<Rendezvous> result;

		result = rendezvousRepository.getRendezvousNoDraftNoDeletedNoAdultsOnlyByCategory(categoryId);

		return result;
	}
	
}
