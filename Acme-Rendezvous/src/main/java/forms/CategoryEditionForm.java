package forms;

import org.hibernate.validator.constraints.NotBlank;


public class CategoryEditionForm {
	public CategoryEditionForm() {
		super();
	}

	private String editedName;
	private String editedDescription;
	
	@NotBlank
	public String getEditedName() {
		return editedName;
	}
	
	public void setEditedName(String editedName) {
		this.editedName = editedName;
	}
	
	@NotBlank
	public String getEditedDescription() {
		return editedDescription;
	}
	
	public void setEditedDescription(String editedDescription) {
		this.editedDescription = editedDescription;
	}
}
