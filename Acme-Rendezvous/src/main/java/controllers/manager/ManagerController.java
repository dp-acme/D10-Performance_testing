package controllers.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ManagerService;
import controllers.AbstractController;
import domain.Manager;
import forms.TermsAndConditionsManagerFormObject;

@Controller
@RequestMapping("/manager")
public class ManagerController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private ManagerService managerService;
	
	// Constructor -----------------------------------------------------------

	public ManagerController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		TermsAndConditionsManagerFormObject userEditForm;
		
		userEditForm = new TermsAndConditionsManagerFormObject();
		result = this.createEditModelAndView(userEditForm);

		
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@ModelAttribute("userEditForm") TermsAndConditionsManagerFormObject userEditForm, BindingResult binding){
		ModelAndView result;
		Manager manager;
	
		
		if(!userEditForm.getTermsAccepted()){
			result = createEditModelAndView(userEditForm);
			result.addObject("showMessageTerms", true);
			return result;
		}
		
		manager = managerService.reconstruct(userEditForm, binding);
		if(binding.hasErrors()){
			result = createEditModelAndView(userEditForm);
		} else { 
			try {
				managerService.save(manager);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(userEditForm, "manager.commit.error");
			}
		}
		return result;
	}
	
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(TermsAndConditionsManagerFormObject formObject) {
		ModelAndView result; 
		
		result = createEditModelAndView(formObject, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(TermsAndConditionsManagerFormObject formObject, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("actor/manager/create");
		result.addObject("userEditForm", formObject);
		result.addObject("message", messageCode);
		result.addObject("requestURI", "manager/create.do");
		
		return result;
	}
	
}
