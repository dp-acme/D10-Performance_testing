/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.configuration;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ConfigurationService;
import controllers.AbstractController;
import domain.Configuration;

@Controller
@RequestMapping("/configuration")
public class ConfigurationController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private ConfigurationService configurationService;
	
	// Constructors -----------------------------------------------------------

	public ConfigurationController() {
		super();
	}
	
	@RequestMapping("/display")
	public ModelAndView display() {
		ModelAndView result;
		Configuration configuration;
		
		configuration = configurationService.findConfiguration();
		result = new ModelAndView("configuration/display");
		result.addObject("configuration", configuration);

		return result;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		Configuration configuration;
		
		configuration = configurationService.findConfiguration();
		
		Assert.notNull(configuration);
		
		result = createEditModelAndView(configuration);

		return result;
	}


	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@ModelAttribute(value="configuration") @Valid Configuration configuration, BindingResult binding) {
		ModelAndView result;
		
		if(binding.hasErrors()){
			result = createEditModelAndView(configuration);
		}else{
			try{
				configurationService.save(configuration);
				result = new ModelAndView("redirect:display.do");
			}catch(Throwable oops){
				result = createEditModelAndView(configuration, "configuration.commit.error");
			}
		}

		return result;
	}


	protected ModelAndView createEditModelAndView(Configuration configuration) {
		ModelAndView result;
		
		result = createEditModelAndView(configuration, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Configuration configuration, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("configuration/edit");
		result.addObject("configuration", configuration);
		
		result.addObject("message", messageCode);

		return result;
	}
	
	

}
