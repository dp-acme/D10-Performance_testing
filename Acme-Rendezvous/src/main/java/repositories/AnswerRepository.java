package repositories;


import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Answer;
import domain.Rendezvous;
import domain.User;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Integer>{
	
	@Query("select a from Answer a where a.question.rendezvous = ?1 and a.user = ?2")
	Collection<Answer> getAnswerOfRendezvousByUser(Rendezvous rendezvous, User user);
	
}