package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class User extends Actor {

	// Constructor
	
	public User() {
		super();
	}

	// Attributes
	
	// Relationships
	
	private Collection<Rendezvous> myRendezvouses;
	private Collection<Rendezvous> myRSVPs;
	
	@Valid
	@NotNull
	@OneToMany(mappedBy = "creator")
	public Collection<Rendezvous> getMyRendezvouses() {
		return myRendezvouses;
	}
	public void setMyRendezvouses(Collection<Rendezvous> myRendezvouses) {
		this.myRendezvouses = myRendezvouses;
	}
	
	@Valid
	@NotNull
	@ManyToMany
	public Collection<Rendezvous> getMyRSVPs() {
		return myRSVPs;
	}
	public void setMyRSVPs(Collection<Rendezvous> myRSVPs) {
		this.myRSVPs = myRSVPs;
	}
	
}
