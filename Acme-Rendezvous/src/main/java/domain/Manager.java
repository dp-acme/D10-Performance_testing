package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {

	
	// Constructor
	
	public Manager() {
		super();
	}
	
	// Attributes
	private String vat;
	
	@NotBlank
	@Pattern(regexp="^[a-zA-Z0-9-]*$")
	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}
	
	
	// Relationships
	private Collection<Service> services;

	@Valid
	@NotNull
	@OneToMany(mappedBy = "manager")
	public Collection<Service> getServices() {
		return services;
	}

	public void setServices(Collection<Service> services) {
		this.services = services;
	}
	
}
