package services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import services.ActorService;
import services.AnswerService;
import services.RendezvousService;
import utilities.AbstractTest;
import domain.Answer;
import domain.Question;
import domain.Rendezvous;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/junit.xml"})
@Transactional
public class AnswerServiceTest extends AbstractTest{

	@Autowired
	private AnswerService answerService;
	
	@Autowired
	private RendezvousService rendezvousService;
		
	@Autowired
	private ActorService actorService;
	
	@SuppressWarnings("unchecked")
	@Test
	public void driverDoRSVP(){
		Object[][] testingData = {
				{"user2", "rendezvous1", Arrays.asList("Respuesta 1", "Respuesta 2"), null}, // Acci�n permitida
				{"user2", "rendezvous1", Arrays.asList(null, "Respuesta 2"), ConstraintViolationException.class}, //Restricci�n del modelo
				{"user2", "rendezvous1", Arrays.asList("Respuesta 1", null), ConstraintViolationException.class},	//Restricci�n del modelo
				{"user2", "rendezvous1", Arrays.asList("", "Respuesta 2"), ConstraintViolationException.class},	//Restricci�n del modelo
				{"user2", "rendezvous1", Arrays.asList("Respuesta 1", ""), ConstraintViolationException.class},	//Restricci�n del modelo
				{"user2", "rendezvous2", Arrays.asList("Respuesta 1", "Respuesta 2"), IllegalArgumentException.class}, //Rendezvous no final
				{"user1", "rendezvous1", Arrays.asList("Respuesta 1", "Respuesta 2"), IllegalArgumentException.class}, //Due�o del rendezvous
				{"user1", "rendezvous3", Arrays.asList("Respuesta 1"), IllegalArgumentException.class}, //Usuario ya unido 
				{"user3", "rendezvous1", Arrays.asList("Respuesta 1", "Respuesta 2"), IllegalArgumentException.class}, //Usuario menor de edad y rendezvous para adultos
				{"user2", "rendezvous6", Arrays.asList("Respuesta 1", "Respuesta 2"), IllegalArgumentException.class}, //Rendezvous marcado como borrado. 
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateDoRSVP((String) testingData[i][0],(String) testingData[i][1], (List<String>) testingData[i][2], (Class<?>) testingData[i][3]);
		}
	}
	
	@Test
	public void driverListAnswers(){
		Object[][] testingData = {
				{null, "rendezvous1", null}, // Acci�n permitida
				{"user1", "rendezvous1", null}, // Acci�n permitida
				{"manager1", "rendezvous1", null},	// Acci�n permitida
		};
		
		for(int i = 0; i<testingData.length; i++){
			templateListAnswers((String) testingData[i][0],(String) testingData[i][1],(Class<?>) testingData[i][2]);
		}
	}
	
	/*Answer the questions that are associated with a rendezvous that he or she's RSVPing
	now.*/
	protected void templateDoRSVP(String username, String rendezvousBeanId, List<String> answers, Class<?> expected){
		Class<?>  caught;
		Rendezvous toRVPS;
		int rendezvousId;
		
		caught = null;

		try{
			this.startTransaction();
			
			super.authenticate(username);
			rendezvousId = super.getEntityId(rendezvousBeanId);
			
			toRVPS = rendezvousService.findOne(rendezvousId);
			
			answerService.RVPS(toRVPS, answers);
			
			Assert.isTrue(toRVPS.getUsers().contains(actorService.findPrincipal()));
			
			super.unauthenticate();
			
		}catch (Throwable e) {
			caught = e.getClass();
		}finally{
			this.rollbackTransaction();
		}
		
		checkExceptions(expected, caught);
	}
	
	/* An actor who is not authenticated must be able to:
	   1. Display information about the users who have RSVPd a rendezvous, which, in turn,
		must show their answers to the questions that the creator has registered.*/
	
	protected void templateListAnswers(String username, String rendezvousBeanId, Class<?> expected){
		Class<?>  caught;
		Rendezvous rendezvous;
		int rendezvousId;
		List<Answer> answers = new ArrayList<Answer>();
		
		caught = null;

		try{
			this.startTransaction();
			
			super.authenticate(username);
			rendezvousId = super.getEntityId(rendezvousBeanId);
			
			rendezvous = rendezvousService.findOne(rendezvousId);
			
			for(Question q : rendezvous.getQuestions()){
				answers.addAll(q.getAnswers());
			}
						
			super.unauthenticate();
			
		}catch (Throwable e) {
			caught = e.getClass();
		}finally{
			this.rollbackTransaction();
		}
		
		checkExceptions(expected, caught);
	}
	
}
