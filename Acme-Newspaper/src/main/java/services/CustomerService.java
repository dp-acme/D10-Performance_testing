package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.CustomerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Customer;
import forms.RegisterCustomer;

@org.springframework.stereotype.Service
@Transactional
public class CustomerService {
	// Managed repository ---------------------------------------------------
		@Autowired
		private CustomerRepository customerRepository;
		
	// Supporting services ---------------------------------------------------
		
	// Constructor ---------------------------------------------------
		
		public CustomerService() {
			super();
		}
	
		// Simple CRUD methods ---------------------------------------------------
		public Customer create() {
			Customer result;
			
			result = new Customer();
			
			return result;
		}
		
		public Customer findOne(int customerId) {
			Assert.isTrue(customerId != 0);
			
			Customer result;
			
			result = customerRepository.findOne(customerId);
			
			return result;
		}
		
		public Collection<Customer> findAll() {		
			Collection<Customer> result;
			
			result = customerRepository.findAll();
			Assert.notNull(result);
			
			return result;
		}
		
		public Customer save(Customer customer) {
			Assert.notNull(customer);
			Customer result;
			UserAccount userAccount;
			
			if(customer.getId()!=0){
				userAccount = LoginService.getPrincipal();
				Assert.isTrue(customer.getUserAccount().equals(userAccount));
			}else{
				String role;
				Md5PasswordEncoder encoder;

				if(SecurityContextHolder.getContext().getAuthentication()!=null
			&&SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray().length>0){
					role = SecurityContextHolder.getContext()
						.getAuthentication().getAuthorities().toArray()[0].toString();
				Assert.isTrue(role.equals("ROLE_ANONYMOUS"));
				}
				encoder = new Md5PasswordEncoder();
				customer.getUserAccount().setPassword(encoder.encodePassword(customer.getUserAccount().getPassword(),null));
			}
			
			result = customerRepository.save(customer);

			return result;
		}
		
		// Other business methods -------------------------------------------------
		
		@Autowired
		private Validator validator;
		
		public Customer reconstruct(RegisterCustomer formObject, BindingResult binding) {
			Customer result;
			Authority authority;
			
			result = create();
			authority = new Authority();
			result.setAddress(formObject.getAddress());
			result.setEmail(formObject.getEmail());
			result.setName(formObject.getName());
			result.setPhone(formObject.getPhone());
			result.setSurname(formObject.getSurname());
			authority.setAuthority(Authority.CUSTOMER);
			formObject.getUserAccount().addAuthority(authority);
			result.setUserAccount(formObject.getUserAccount());
			
			validator.validate(formObject, binding);
			
			return result;
		}
		
		public void flush() {
			customerRepository.flush();
		}
}
