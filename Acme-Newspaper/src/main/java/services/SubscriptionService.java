package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.SubscriptionRepository;
import domain.Actor;
import domain.Customer;
import domain.Newspaper;
import domain.Subscription;

@Service
@Transactional
public class SubscriptionService {

	// Managed repository ---------------------------------------------------

	@Autowired
	private SubscriptionRepository subscriptionRepository;

	// Supporting services ---------------------------------------------------

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	// Constructor------------------------------------------------------------------------

	public SubscriptionService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------

	public Subscription create(Newspaper newspaper) {
		Assert.notNull(newspaper);
		
		// Comprobamos que el usuario logeado es un customer
		Actor actor;
		actor = actorService.findPrincipal();
		Assert.isInstanceOf(Customer.class, actor);
		
		Customer customer;
		customer = (Customer) actor;
		
		Assert.isTrue(!newspaper.isDraft());
		Assert.isTrue(newspaper.isIsPrivate());
		Assert.isTrue(newspaper.getPublicationDate().before(new Date()));
		Assert.isNull(getSubscriptionFromCustomerAndNewspaper(customer.getId(), newspaper.getId()));

		// Creamos el resultado
		Subscription result;
		result = new Subscription();
		
		// Inicializamos los atributos
		result.setCustomer(customer);
		result.setNewspaper(newspaper);

		return result;
	}

	public Subscription findOne(int subscriptionId) {
		// Creamos el objeto a devolver
		Subscription result;
		
		// Cogemos del repositorio el Subscription que le pasamos como parametro
		result = this.subscriptionRepository.findOne(subscriptionId);
		Assert.notNull(result);
		
		return result;
	}

	public Collection<Subscription> findAll() {
		// Creamos el objeto a devolver
		Collection<Subscription> result;
		
		// Cogemos del repositorio los Subscription
		result = this.subscriptionRepository.findAll();
		
		return result;
	}

	public Subscription save(Subscription subscription) {
		Assert.notNull(subscription);
		Assert.isTrue(!subscription.getNewspaper().isDraft());
		Assert.isTrue(subscription.getNewspaper().isIsPrivate());
		Assert.isTrue(subscription.getNewspaper().getPublicationDate().before(new Date()));
		
		Actor actor;
		actor = actorService.findPrincipal();
		
		Assert.isInstanceOf(Customer.class, actor);
		Assert.isTrue(actor.getUserAccount().equals(subscription.getCustomer().getUserAccount()));
		
		Assert.isNull(getSubscriptionFromCustomerAndNewspaper(actor.getId(), subscription.getNewspaper().getId()));
		
		// Creamos el objeto a devolver
		Subscription result;

		// Actualizamos la subscription
		result = this.subscriptionRepository.save(subscription);

		return result;
	}

	// Other business methods ---------------------------------------------------
	
	@Autowired
	private Validator validator;
	
	public Subscription reconstruct(Subscription subscription, BindingResult binding, int newspaperId) {
		Newspaper newspaper;
		Customer customer;
		
		newspaper = newspaperService.findOne(newspaperId);
		Assert.notNull(newspaper);
		
		customer = (Customer) actorService.findPrincipal();
		Assert.notNull(newspaper);
		
		subscription.setNewspaper(newspaper);
		subscription.setCustomer(customer);
		
		validator.validate(subscription, binding);
		
		return subscription;
	}
	
	public Subscription getSubscriptionFromCustomerAndNewspaper(int customerId, int newspaperId) {
		Subscription result;
		
		result = subscriptionRepository.getSubscriptionFromCustomerAndNewspaper(customerId, newspaperId);
		
		return result;
	}
	
	public Collection<Subscription> getSubscriptionsFromCustomer(int customerId) {
		Collection<Subscription> result;
		
		result = subscriptionRepository.getSubscriptionsFromCustomer(customerId);
		
		return result;
	}
	
	public Collection<Subscription> getSubscriptionsFromNewspaper(int newspaperId) {
		Collection<Subscription> result;
		
		result = subscriptionRepository.getSubscriptionsFromNewspaper(newspaperId);
		
		return result;
	}

	public void flush()
	{
		subscriptionRepository.flush();
	}
	
}
