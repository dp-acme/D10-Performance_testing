package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Article;

@Component
@Transactional
public class ArticleToStringConverter implements Converter<Article, String> {

	@Override
	public String convert(Article source) {
		String result;
		if (source == null)
			result = null;
		else
			result = String.valueOf(source.getId());

		return result;
	}

}
