package controllers.subscription;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.NewspaperService;
import services.SubscriptionService;
import controllers.AbstractController;
import domain.Actor;
import domain.Customer;
import domain.Newspaper;
import domain.Subscription;

@Controller
@RequestMapping("/subscription/customer")
public class SubscriptionCustomerController extends AbstractController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private NewspaperService newspaperService;
	
	// Constructor -----------------------------------------------------------

	public SubscriptionCustomerController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;

		Actor actor;
		Customer customer;
		Collection<Subscription> subscriptions;
		
		actor = actorService.findPrincipal();
		Assert.isInstanceOf(Customer.class, actor);
		customer = (Customer) actor;
		
		subscriptions = subscriptionService.getSubscriptionsFromCustomer(customer.getId());
		
		result = new ModelAndView("subscription/list");
		result.addObject("subscriptions", subscriptions);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true) int newspaperId){
		ModelAndView result;
		Subscription subscription;
		Newspaper newspaper;
		
		newspaper = newspaperService.findOne(newspaperId);
		Assert.notNull(newspaper);
		
		subscription = subscriptionService.create(newspaper);
		
		result = this.createEditModelAndView(subscription);

		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@RequestParam(required = true, value = "newspaperId") int newspaperId , 
			@ModelAttribute(value="subscription") Subscription subscription, BindingResult binding){
		ModelAndView result;
		
		subscription = subscriptionService.reconstruct(subscription, binding, newspaperId);
		
		if(binding.hasErrors()){
			result = createEditModelAndView(subscription, null);
		} else { 
			try {
				subscription = subscriptionService.save(subscription);
				
				result = new ModelAndView("redirect:/newspaper/display.do?newspaperId=" + subscription.getNewspaper().getId());
				
			} catch (Throwable oops) {
				result = createEditModelAndView(subscription, "subscription.commit.error");
			}
		}
		return result;
	}
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(Subscription subscription) {
		ModelAndView result;
		
		result = createEditModelAndView(subscription, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(Subscription subscription, String messageCode) {
		ModelAndView result;
		
		result = new ModelAndView("subscription/create");
		result.addObject("subscription", subscription);
		result.addObject("message", messageCode);
		
		return result;
	}

}
