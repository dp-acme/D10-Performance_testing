package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Article extends DomainEntity {
	
	// Constructor
	
	public Article() {
		super();
	}
	
	// Attributes

	private String title;
	private String summary;
	private String body;
	private Collection<String> pictures;
	private Date moment;
	private Boolean draft;
	private Boolean containsTabooWord;

	@NotNull
	public Boolean getContainsTabooWord() {
		return containsTabooWord;
	}
	
	public void setContainsTabooWord(Boolean containsTabooWord) {
		this.containsTabooWord = containsTabooWord;
	}
	
	
	@NotBlank
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	@NotBlank
	public String getSummary() {
		return summary;
	}
	
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	@NotBlank
	public String getBody() {
		return body;
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
	@ElementCollection
	public Collection<String> getPictures() {
		return pictures;
	}
	
	public void setPictures(Collection<String> pictures) {
		this.pictures = pictures;
	}
	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getMoment() {
		return moment;
	}
	
	public void setMoment(Date moment) {
		this.moment = moment;
	}
	
	@NotNull
	public Boolean getDraft() {
		return draft;
	}
	
	public void setDraft(Boolean draft) {
		this.draft = draft;
	}
	
	// Relationships
	
	private Article article;
	private Newspaper newspaper;
	
	@Valid
	@ManyToOne(optional = true)
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	@Valid
	@ManyToOne(optional = true)	
	public Newspaper getNewspaper() {
		return newspaper;
	}

	public void setNewspaper(Newspaper newspaper) {
		this.newspaper = newspaper;
	}
}
