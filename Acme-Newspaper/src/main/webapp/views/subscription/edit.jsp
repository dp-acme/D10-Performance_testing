<%--
 * request/edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="subscription/customer/create.do?newspaperId=${param.newspaperId}" modelAttribute="subscription" >

	<!-- Indicamos los campos de los formularios -->

	<acme:textbox code="edit.holderName" path="holderName" required="required" />
	<br />
	
	<acme:textbox code="edit.brandName" path="brandName" required="required" />
	<br />

	<acme:textbox code="edit.creditCardNumber" path="creditCardNumber" required="required" />
	<br />

	<acme:textbox type="number" code="edit.expirationMonth" path="expirationMonth" required="required" placeholderCode="edit.expirationMonth.placeholder" />
	<br />
	
	<acme:textbox type="number" code="edit.expirationYear" path="expirationYear" required="required" placeholderCode="edit.expirationYear.placeholder" />
	<br />
	
	<acme:textbox code="edit.cvv" path="cvv" required="required" placeholderCode="edit.cvv.placeholder" />
	<br />
	
	<!-- Botones del formulario -->
	<acme:submit name="save" code="edit.save" />
	<acme:cancel url="newspaper/display.do?newspaperId=${param.newspaperId}" code="edit.cancel" />

</form:form>