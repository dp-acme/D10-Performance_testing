<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ page import="java.text.DecimalFormat" %>

<h2><spring:message code="dashboard.statistics" /></h2>

<ul>

	<li>	
		<b><spring:message code="dashboard.ratioOfCreators" />:</b>
		<jstl:out value="${ratioOfCreators}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.ratioOfArticleCreators" />:</b>
		<jstl:out value="${ratioOfArticleCreators}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.getAvgFollowUpsPerArticle" />:</b>
		<jstl:out value="${getAvgFollowUpsPerArticle}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.ratioUsersWithOver75PercentAvgChirps" />:</b>
		<jstl:out value="${ratioUsersWithOver75PercentAvgChirps}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.ratioPublicOverPrivateNewspapers" />:</b>
		<jstl:out value="${ratioPublicOverPrivateNewspapers}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.getAvgArticlesPerPrivateNewspaper" />:</b>
		<jstl:out value="${getAvgArticlesPerPrivateNewspaper}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.getAvgArticlesPerPublicNewspaper" />:</b>
		<jstl:out value="${getAvgArticlesPerPublicNewspaper}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.getAvgRatioPrivateOverPublicNewspaperPerUser" />:</b>
		<jstl:out value="${getAvgRatioPrivateOverPublicNewspaperPerUser}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.getAvgFollowupsPerArticlePlus1Week" />:</b>
		<jstl:out value="${getAvgFollowupsPerArticlePlus1Week}" />
	</li>
</ul>

<ul>

	<li>	
		<b><spring:message code="dashboard.getAvgFollowupsPerArticlePlus2Weeks" />:</b>
		<jstl:out value="${getAvgFollowupsPerArticlePlus2Weeks}" />
	</li>
</ul>

<table> 
	<tr style="background-color: yellow;">
		<th><spring:message code="dashboard.table.property" /></th>
		<th><spring:message code="dashboard.table.average" /></th>
		<th><spring:message code="dashboard.table.sd" /></th>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.getAvgSdNewspapersPerUser" /></td>
		<td><jstl:out value="${getAvgSdNewspapersPerUser[0]}" /></td>
		<td><jstl:out value="${getAvgSdNewspapersPerUser[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.getAvgSdArticlesPerUser" /></td>
		<td><jstl:out value="${getAvgSdArticlesPerUser[0]}" /></td>
		<td><jstl:out value="${getAvgSdArticlesPerUser[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.getAvgSdArticlesPerNewspaper" /></td>
		<td><jstl:out value="${getAvgSdArticlesPerNewspaper[0]}" /></td>
		<td><jstl:out value="${getAvgSdArticlesPerNewspaper[1]}" /></td>
	</tr>
	
	<tr>
		<td><spring:message code="dashboard.getAvgSdChirpsPerUser" /></td>
		<td><jstl:out value="${getAvgSdChirpsPerUser[0]}" /></td>
		<td><jstl:out value="${getAvgSdChirpsPerUser[1]}" /></td>
	</tr>
	</table>
<br>

<br>

	<spring:message code="dashboard.dateTimeFormat" var = "dateTimePattern" />

<h2><spring:message code="dashboard.getNewspaperWith10PercentLessArticlesThanAvg" /></h2>


<display:table id="row" name="getNewspaperWith10PercentLessArticlesThanAvg" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="title" titleKey="dashboard.table.newspaper.title" sortable="true" />
	<display:column property="description" titleKey="dashboard.table.newspaper.description" sortable="true" />
	<spring:message code="dashboard.dateTimeFormat" var = "dateTimePattern" />
	<display:column property="publicationDate" titleKey="dashboard.table.newspaper.publicationDate" format="{0,date,${dateTimePattern}}" sortable="true" /> 
</display:table>

<h2><spring:message code="dashboard.getNewspaperWith10PercentMoreArticlesThanAvg" /></h2>


<display:table id="row" name="getNewspaperWith10PercentMoreArticlesThanAvg" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="title" titleKey="dashboard.table.newspaper.title" sortable="true" />
	<display:column property="description" titleKey="dashboard.table.newspaper.description" sortable="true" />
	<spring:message code="dashboard.dateTimeFormat" var = "dateTimePattern" />
	<display:column property="publicationDate" titleKey="dashboard.table.newspaper.publicationDate" format="{0,date,${dateTimePattern}}" sortable="true" /> 
</display:table>

<h2><spring:message code="dashboard.ratioOfSubscribersPerNewspaper" /></h2>

<display:table id="row" name="ratioOfSubscribersPerNewspaper" requestURI="dashboard/display.do" pagesize="5">
	<display:column property="[0].title" titleKey="dashboard.table.newspaper.title" sortable="true" />
	<display:column property="[1]" titleKey="dashboard.table.newspaper.ratio" sortable="true" />
</display:table>

<br>
