<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="org.springframework.web.util.HtmlUtils"%>
<%@page import="javassist.compiler.ast.Keyword"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt"	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<script>
	function search(){
		var keyword = document.getElementById("keywordInput").value;
		
		relativeRedir("article/list.do?keyword=" + keyword);
	}
</script>

<input type="text" id="keywordInput"/>
<spring:message var="searchButton" code="article.button.search" />
<input type="button" value="${searchButton}" onclick="javascript: search();"/>

<br>

<%
	String requestURI = "article/list.do";

	if(request.getAttribute("keyword") != null){
		request.getAttribute("keyword");
		requestURI += "?keyword=" + HtmlUtils.htmlEscape((String) request.getAttribute("keyword"));
	}
	
	pageContext.setAttribute("requestURI", requestURI);
%>

<spring:message var="formatDatePattern" code="article.dateFormat" />

<display:table name="articles" id="row" requestURI="${requestURI}" pagesize="5">
	<spring:message code="article.title" var="titleHeader"/>
	<display:column title="${titleHeader}">
		<a href="article/display.do?articleId=${row.id}">
			<jstl:out value="${row.title}" />
		</a>
	</display:column>
	
	<spring:message code="article.summary" var="summaryHeader"/>
	<display:column property="summary" title="${summaryHeader}"/>
	
	<spring:message code="article.moment" var="momentHeader"/>
	<display:column title="${momentHeader}" >
		<fmt:formatDate value="${row.moment}" pattern="${formatDatePattern}" />
	</display:column>
</display:table>